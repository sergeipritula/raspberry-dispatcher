﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using System.Linq;
using System.Security.Cryptography;
using System.Diagnostics;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Net.Cache;
using MySql.Data.MySqlClient;

/// <summary>  
///  This class performs using RaspberryPi I/O ports.</summary>  
///  <remarks>For controlling GPIO pin you need create object of this class with GPIO pin number parameter, then you need to call : <para />
///  1) GPIOExport() <para />
///  2) GPIOSetDirection(string dir) <para />
///  3) Controll GPIO pin using GPIOSetValue(string val) or GPIOGetValue()</remarks>
public class GPIO
{
    /// <summary>
    /// Store for the pin number of I/O port</summary>
    private String GpioPin = "1";
    private const String GpioExportPath = "/sys/class/gpio/export";
    private const String GpioUnexportPath = "/sys/class/gpio/unexport";
    /// <summary>
    /// The class constructor.</summary>
    /// <param name="GpioPin">
    /// gnum - GPIO pin number need to control.
    /// </param>
    public GPIO(string GpioPin)
    {
        this.GpioPin = GpioPin;
    }

    /// <summary>
    /// Export current gpio to "/sys/class/gpio/export".</summary>
    public int GPIOExport()
    {
        if (File.Exists(GpioExportPath))
            File.WriteAllText(GpioExportPath, GpioPin);
        else
            throw new FileNotFoundException();
        return 0;
    }

    /// <summary>
    /// Unexport current gpio to "/sys/class/gpio/unexport".</summary>
    public int GPIOUnexport()
    {
        if (File.Exists(GpioUnexportPath))
            File.WriteAllText(GpioUnexportPath, GpioPin);
        else
            throw new FileNotFoundException();
        return 0;
    }

    /// <summary>
    /// Set direction for current GPIO pin.</summary>
    /// <param name="dir">
    /// Set dirrection for GPIO object <para />
    /// "in" - set direction pin to be as input (use GPIOGetVlaue()) <para />
    /// "out" - set direction pin to be as output (use GPIOSetVlaue(string value)) 
    /// </param>
    public int GPIOSetDirection(string dir)
    {
        string GpioDirectionPath = "/sys/class/gpio/gpio" + GpioPin + "/direction";
        if (File.Exists(GpioDirectionPath))
            File.WriteAllText(GpioDirectionPath, dir);
        else
            throw new FileNotFoundException();
        return 0;
    }

    /// <summary>
    /// Set GPIO pin to 1 or 0.</summary>
    /// <param name="Value">
    /// Set value for GPIO object <para />
    /// "1" - set value pin to high<para />
    /// "0" - set value pin to low
    /// </param>
    public int GPIOSetValue(string Value)
    {
        string GpioValuePath = "/sys/class/gpio/gpio" + GpioPin + "/value";
        if (File.Exists(GpioValuePath))
            File.WriteAllText(GpioValuePath, Value);
        else
            throw new FileNotFoundException();
        return 0;
    }

    /// <summary>
    /// Get GPIO current value (0 or 1).</summary>
    public int GPIOGetValue()
    {
        string GpioValuePath = "/sys/class/gpio/gpio" + GpioPin + "/value";
        if (File.Exists(GpioValuePath))
            return Convert.ToInt32(File.ReadAllText(GpioValuePath));
        else
            throw new FileNotFoundException();
    }
}

public class CheckSum
{
    private CheckSum()
    {

    }

    public static bool CheckCRC16(byte[] data)
    {
        int ReceivedCRC = (data[data.Length - 1] << 8) | data[data.Length - 2];
        Console.WriteLine("CheckSum ReceivedCRC => " + ReceivedCRC);
        Console.WriteLine("CheckSum CalculatedCRC => " + CRC16(data, data.Length));
        return ReceivedCRC == CRC16(data, data.Length) ? true : false;
    }

    public static short CRC16(byte[] data, int size)
    {
        int Sum = 0;
        int byte_cnt = size - 2;
        int shift_cnt;
        Sum = 0xFFFF;
        int i = 0;
        for (; byte_cnt > 0; byte_cnt--)
        {
            Sum = (ushort)((Sum / 256) * 256 + ((Sum % 256) ^ data[i++]));
            for (shift_cnt = 0; shift_cnt < 8; shift_cnt++)
            {
                if ((Sum & 0x1) == 1) Sum = ((Sum >> 1) ^ 0xa001);
                else Sum >>= 1;
            }
        }
        byte[] byteArray = BitConverter.GetBytes((short)Sum);
        data[data.Length - 2] = byteArray[0];
        data[data.Length - 1] = byteArray[1];
        return BitConverter.ToInt16(new byte[2] { data[data.Length - 2], data[data.Length - 1] }, 0);
    }

    public static bool CheckXorCRC(byte[] pedReponse)
    {
        int crc = 0;

        for (int i = 0; i < pedReponse.Length - 2; i++)
        {
            crc ^= pedReponse[i];
        }

        if (pedReponse[pedReponse.Length - 1] == crc)
            return true;
        else
            return false;
    }
}


public class ComPortControll
{
    private ComPortControll()
    {

    }

    public void WritePacket(SerialPort port, String data)
    {
        port.Write(data);
        Thread.Sleep(1000);
    }

    public static void WritePacket(SerialPort port, byte[] data)
    {
        port.Write(data, 0, data.Length);
        Thread.Sleep(1000);
    }

    public static byte[] ReadPacket(SerialPort port)
    {
        int SerialPortAvailableSize = port.BytesToRead;
        byte[] Receive = new byte[SerialPortAvailableSize];
        port.Read(Receive, 0, SerialPortAvailableSize);
        return Receive;
    }
}


/// <summary>  
///  This class is using for store commands receiving from Server.exe.</summary>  
public class Command
{
    /// <summary>
    /// The date for which you want to read the archive
    /// </summary>   
    public DateTime DateReadFor;

    /// <summary>
    /// CommandID from Server.exe
    /// </summary>   
    public int CommandID;

    /// <summary>
    /// Device serial number
    /// </summary>   
    public String SerialNumber;

    /// <summary>  
    ///  The class construct.</summary>  
    /// <param name="SerialNumber">
    /// Device serial number
    /// </param>   
    /// <param name="CommandID">
    /// CommandID from Server.exe
    /// </param>   
    /// <param name="DateReadFor">
    ///  The date for which you want to read the archive
    /// </param>   
    public Command(String SerialNumber, int CommandID, DateTime DateReadFor)
    {
        this.CommandID = CommandID;
        this.SerialNumber = SerialNumber;
        this.DateReadFor = DateReadFor;
    }
}

/// <summary>  
///  This class is a model to store values.</summary>
public class Parameters
{
    /// <summary>
    /// Using to store paramter's value</summary>
    public double ParameterValue { get; set; }

    /// <summary>
    /// Parameter obisCode</summary>
    public int ParameterObisCode { get; set; }

    /// <summary>
    /// Quality of reading value</summary>
    public int ParameterQuality { get; set; }

    /// <summary>
    ///Value parameterNumber</summary>
    public int ParameterNumber { get; set; }

    /// <summary>
    ///Identifer of device</summary>
    public String SerialNumber { get; set; }

    /// <summary>
    /// Time of value reading</summary>
    public DateTime DateOfRead { get; set; }

    /// <summary>
    /// Device EquipmentID</summary>
    public int ParameterEquipmentTypeID { get; set; }

    /// <summary>
    /// Empty constructor for initialization
    /// </summary>
    public Parameters()
    {
        ParameterQuality = 2;
        DateOfRead = DateTime.Now;
    }

    /// <summary>
    /// Constructor with parameters
    /// </summary>
    public Parameters(String Identifier, int ObisCode, int ParameterNumber, double Value, int EquipmentTypeID)
        : this()
    {
        ParameterValue = Value;
        ParameterObisCode = ObisCode;
        this.ParameterNumber = ParameterNumber;
        SerialNumber = Identifier;
        ParameterEquipmentTypeID = EquipmentTypeID;
    }

    /// <summary>
    /// Constructor with parameters for archive date
    /// </summary>
    public Parameters(String Identifier, int ObisCode, int ParameterNumber, double Value, int EquipmentTypeID, DateTime DateOfRead)
        : this(Identifier, ObisCode, ParameterNumber, Value, EquipmentTypeID)
    {
        this.DateOfRead = DateOfRead;
    }

    /// <summary>
    /// Constructor initialized with same object
    /// </summary>
    public Parameters(Parameters Parameter)
    {
        SerialNumber = Parameter.SerialNumber;
        ParameterObisCode = Parameter.ParameterObisCode;
        ParameterNumber = Parameter.ParameterNumber;
        ParameterValue = Parameter.ParameterValue;
        ParameterEquipmentTypeID = Parameter.ParameterEquipmentTypeID;
        DateOfRead = Parameter.DateOfRead;
    }
}

public class MeteoStation
{
    private List<Parameters> Data = new List<Parameters>();
    private String ID = String.Empty;
    private int EquipmentTypeID = 0;
    private String ComName = String.Empty;
    private String RS_Type = String.Empty;
    private int baudRate = 0;


    public MeteoStation(String ID, int EquipmentTypeID, String comName, int baud, String rsType)
    {
        this.ID = ID;
        this.EquipmentTypeID = EquipmentTypeID;
        this.ComName = comName;
        this.baudRate = baud;
        this.RS_Type = rsType;
    }

    public void ReadData()
    {
        Console.WriteLine("MeteoStation --> /dev/" + ComName);
        Data.Clear();

        SerialPort Serial = new SerialPort("/dev/" + ComName, 9600, Parity.None, 8, StopBits.One);

        if (RS_Type.Contains("232"))
            Serial.Handshake = Handshake.RequestToSend;
        else
            Serial.Handshake = Handshake.None;

        Serial.ReadTimeout = 15000;

        try
        {
            if (!Serial.IsOpen)
                Serial.Open();

            byte[] receive = ComPortControll.ReadPacket(Serial);
            Console.WriteLine("MeteoStation -->> receive Length : " + receive.Length);
            for (int i = 0; i < receive.Length; i++)
            {
                Console.Write("{0:X} ", receive[i]);
            }
            Console.WriteLine();

            if (IsCRC8(receive))
            {
                if (receive.Length == 17)
                {
                    MeteoData rezult = (ParseData(receive));
                    SeeMeteoData(rezult);
                    MeteoDataToList(rezult);
                }
            }
            else
            {
                Console.WriteLine("MeteoStation --> CRC Error");
            }

            Serial.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("MeteoStation -->> " + ex.Message);
        }
    }

    public void MeteoDataToList(MeteoData values)
    {
        Data = new List<Parameters>
        {
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 999, 0, values.SC, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 120, 0, values.DIR, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 101, 0, values.LB, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 95, 0, values.TMP, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 121, 0, values.HM, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 122, 0, values.WSP, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 123, 0, values.GUST, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 124, 0, values.RAIN, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 125, 0, values.UVI, EquipmentTypeID),
            new Parameters(Program.RaspberryID + "-" + ID.ToString(), 46, 0, values.LIGHT, EquipmentTypeID)
        };
    }

    public void SeeMeteoData(MeteoData meteoStationData)
    {
        Console.WriteLine("SecurityCode : " + meteoStationData.SC);
        Console.WriteLine("WindDirection: " + meteoStationData.DIR);
        Console.WriteLine("isLowBattery : " + meteoStationData.LB);
        Console.WriteLine("temperature  : " + meteoStationData.TMP);
        Console.WriteLine("Humudity     : " + meteoStationData.HM);
        Console.WriteLine("wind speed   : " + meteoStationData.WSP);
        Console.WriteLine("gust speed   : " + meteoStationData.GUST);
        Console.WriteLine("rainfall     : " + meteoStationData.RAIN);
        Console.WriteLine("UV           : " + meteoStationData.UVI);
        Console.WriteLine("Light        : " + meteoStationData.LIGHT);
        Console.WriteLine();
    }

    public MeteoData ParseData(byte[] r)
    {
        MeteoData d = new MeteoData()
        {
            SC = r[1]
        };
        byte[] dir = new byte[2];
        dir[0] = r[2];
        dir[1] = (byte)((int)r[3] & 128); //
        if (dir[1] != 0)
        {
            dir[1] = 1;
        }
        int dir1 = dir[0] | (dir[1] << 8);
        if (dir1 != 0x1ff)
            d.DIR = dir1;
        else
            d.DIR = -1;

        d.LB = r[3] & 8;
        if (d.LB != 0) d.LB = 1;

        byte tmp = (byte)((int)r[3] & 7);
        d.TMP = r[4] | (tmp << 8);
        if (d.TMP != 0x7ff)
        {
            d.TMP = (d.TMP - 400) / 10.0;
        }
        else d.TMP = -1;

        if (r[5] != 0xff) d.HM = r[5];
        else d.HM = -1;


        byte wsp = (byte)((int)r[3] & 16);
        d.WSP = r[6] | (wsp << 4); //( (wsp >> 4) <<8 )
        if (d.WSP == 0x1ff) d.WSP = -1;

        if (r[7] != 0xff) d.GUST = r[7];
        else d.GUST = -1;

        d.RAIN = r[9] | (r[8] << 8);

        d.UVI = r[11] | (r[10] << 8);

        d.LIGHT = r[14] | (r[13] << 8) | (r[12] << 16);

        byte CRC = r[15];
        byte CHECKSUM = r[16];
        return d;
    }

    public static bool IsCRC8(byte[] data)
    {
        byte crc = 0;
        if (data != null && data.Length > 0)
        {
            for (int i = 0; i < data.Length - 1; i++)
            {
                crc += data[i];
            }
        }
        if (data[data.Length - 1] == crc)
            return true;
        else
            return false;
    }

    public static bool IsCRC16(byte[] data) // Функция нахождения контрольной сумы
    {
        int Sum = 0;
        int byte_cnt = data.Length - 2;
        int shift_cnt;
        Sum = (ushort)0xffff;
        int i = 0;
        for (; byte_cnt > 0; byte_cnt--)
        {
            Sum = (ushort)((Sum / 256) * 256 + ((Sum % 256) ^ data[i++]));
            for (shift_cnt = 0; shift_cnt < 8; shift_cnt++)
            {
                if ((Sum & 0x1) == 1) Sum = ((Sum >> 1) ^ 0xa001);
                else Sum >>= 1;
            }
        }
        byte[] byteArray = BitConverter.GetBytes((short)Sum);
        if (data[data.Length - 2] == byteArray[0] && data[data.Length - 1] == byteArray[1])
            return true;
        else return false;
    }

    [Serializable]
    public class MeteoData
    {
        public double SC;    //SecurityCode
        public double DIR;   //WindDirection
        public int LB;       // isLowBattery
                             // int WSP; //Bit9 of wind speed
        public double TMP;   //temperature
        public double HM;    //Humudity
        public double WSP;   //wind speed
        public double GUST;  //gust speed
        public double RAIN;  //rainfall
        public double UVI;   //UV
        public double LIGHT; //Light

        public MeteoData()
        {

        }
    }

    public List<String> ExtractCurrentUserId(string inputData)
    {
        List<String> resultData = new List<string>();
        Regex matchData = new Regex("/$*");
        MatchCollection matches = Regex.Matches(inputData, matchData.ToString());

        for (int i = 0; i < matches.Count; i++)
        {
            resultData.Add(matches[i].Value);
        }

        return resultData;
    }

    public List<Parameters> GetDataList()
    {
        if (Data.Any())
        {
            List<Parameters> Temp = new List<Parameters>(Data);
            Data.Clear();
            return Temp;
        }

        return new List<Parameters>();
    }

}


public class Dozor
{
    private List<Parameters> Data = new List<Parameters>();
    private readonly byte[] DOZOR_DATA_REQUEST = { 0x00, MODBUS_READ_FUNCTION, 0x00, 0x00, 0x00, 20, 0x00, 0x00 };
    private const byte MODBUS_READ_FUNCTION = 0x03;
    private const String TAG = ">>>   Dozor ";

    private String SerialNumber = String.Empty;
    private int ID = 0;
    private int EquipmentTypeID = 0;
    private String SerialName = String.Empty;
    private String RsType = String.Empty;
    private int BaudRate = 0;

    public Dozor(String SerialNumber, int ID, int EquipmentTypeID, String SerialName, int BaudRate, String RsType)
    {
        this.SerialName = SerialName;
        this.BaudRate = BaudRate;
        this.RsType = RsType;

        this.SerialNumber = SerialNumber;
        this.ID = ID;
        DOZOR_DATA_REQUEST[0] = (byte)ID;
        this.EquipmentTypeID = EquipmentTypeID;
        CheckSum.CRC16(DOZOR_DATA_REQUEST, DOZOR_DATA_REQUEST.Length);
    }

    public void ReadData()
    {
        Console.WriteLine(TAG + "--> /dev/" + SerialName);
        Data.Clear();

        SerialPort Serial = new SerialPort("/dev/" + SerialName, BaudRate, Parity.None, 8, StopBits.One)
        {
            ReadTimeout = 2000,
            WriteTimeout = 2000
        };

        if (RsType.Contains("232"))
            Serial.Handshake = Handshake.RequestToSend;
        else
            Serial.Handshake = Handshake.None;

        if (!Serial.IsOpen)
            Serial.Open();

        try
        {
            for (int Channel = 5; Channel >= 1; Channel--)
            {
                try
                {
                    DOZOR_DATA_REQUEST[5] = (byte)(Channel * 2);
                    CheckSum.CRC16(DOZOR_DATA_REQUEST, DOZOR_DATA_REQUEST.Length);

                    ComPortControll.WritePacket(Serial, DOZOR_DATA_REQUEST);
                    byte[] Receive = ComPortControll.ReadPacket(Serial);

                    if (!CheckSum.CheckCRC16(Receive))
                    {
                        Console.WriteLine(TAG + " CRC error");
                        throw new Exception("Bad CRC");
                    }

                    Console.WriteLine(TAG + " Response Length " + Receive.Length);

                    if (Receive[1] == 0x83)
                    {
                        Console.WriteLine(TAG + " error request");
                        Thread.Sleep(500);
                        continue;
                    }
                    else
                        ParseResponse(Receive.Skip(3).Take(Receive[2]).ToArray(), Channel);
                }
                catch (Exception e)
                {
                    continue;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(TAG + " " + ex.Message);
        }
        finally
        {
            Serial.Close();
        }
    }

    public void ParseResponse(byte[] data, int channelsCount)
    {
        for (int i = 0; i < channelsCount; i++)
        {
            byte[] temp = data.Skip(i * 4).Take(4).ToArray();
            byte[] concentration_array = temp.Skip(2).Take(2).ToArray();

            int concentration = (concentration_array[0] << 8) | concentration_array[1];
            byte thresholdOne = (byte)(temp[1] & 0x01);
            byte thresholdTwo = (byte)((temp[1] & 0x02) >> 1);
            byte thresholdThree = (byte)((temp[1] & 0x04) >> 2);
            byte sensorType = (byte)((temp[1] & 0x38) >> 3);
            byte concentrationAddition = (byte)((temp[1] & 0xC0) >> 6);
            byte measureType = (byte)((temp[0] & 0x03));

            Console.WriteLine(TAG + " -->> concentration : " + concentration);
            Console.WriteLine(TAG + " -->> thresholdOne : " + thresholdOne);
            Console.WriteLine(TAG + " -->> thresholdTwo : " + thresholdTwo);
            Console.WriteLine(TAG + " -->> thresholdThree : " + thresholdThree);
            Console.WriteLine(TAG + " -->> sensorType : " + sensorType);
            Console.WriteLine(TAG + " -->> concentrationAddition : " + concentrationAddition);
            Console.WriteLine(TAG + " -->> measureType : " + measureType);

            switch (concentrationAddition)
            {
                case 0:
                    Data.Add(new Parameters(SerialNumber, 130, i, concentration, this.EquipmentTypeID));
                    break;
                case 1:
                    Data.Add(new Parameters(SerialNumber, 130, i, concentration / 10.0, this.EquipmentTypeID));
                    break;
                case 2:
                    Data.Add(new Parameters(SerialNumber, 130, i, concentration / 100.0, this.EquipmentTypeID));
                    break;
                case 3:
                    Data.Add(new Parameters(SerialNumber, 130, i, concentration / 1000.0, this.EquipmentTypeID));
                    break;
            }

            Data.Add(new Parameters(SerialNumber, 126, (i * 3), thresholdOne, this.EquipmentTypeID));
            Data.Add(new Parameters(SerialNumber, 126, (i * 3) + 1, thresholdTwo, this.EquipmentTypeID));
            Data.Add(new Parameters(SerialNumber, 126, (i * 3) + 2, thresholdThree, this.EquipmentTypeID));
            Data.Add(new Parameters(SerialNumber, 127, i, sensorType, this.EquipmentTypeID));
            Data.Add(new Parameters(SerialNumber, 128, i, measureType, this.EquipmentTypeID));
            Data.Add(new Parameters(SerialNumber, 129, i, concentrationAddition, this.EquipmentTypeID));
        }
    }

    public List<Parameters> GetDataList()
    {
        if (Data.Any())
        {
            List<Parameters> Temp = new List<Parameters>(Data);
            Data.Clear();
            return Temp;
        }

        return new List<Parameters>();
    }
}

public class Dozimeter
{
    private List<Parameters> Data = new List<Parameters>();
    private const String TAG = ">>>   Dozimeter ";

    private const int WideRequestAddres = 0x0F;
    private String SerialNumber = String.Empty;
    private int ID = 0;
    private int EquipmentTypeID = 0;
    private String SerialName = String.Empty;
    private String RsType = String.Empty;
    private int BaudRate = 0;

    public Dozimeter(String SerialNumber, int ID, int EquipmentTypeID, String SerialName, int BaudRate, String RsType)
    {
        this.SerialName = SerialName;
        this.BaudRate = BaudRate;
        this.RsType = RsType;

        this.SerialNumber = SerialNumber;
        this.ID = ID;
        this.EquipmentTypeID = EquipmentTypeID;
    }

    public void ReadData()
    {
        Console.WriteLine(TAG + "--> /dev/" + SerialName);
        Data.Clear();

        SerialPort Serial = new SerialPort("/dev/" + SerialName, BaudRate, Parity.None, 8, StopBits.One)
        {
            ReadTimeout = 2000,
            WriteTimeout = 2000
        };

        if (RsType.Contains("232"))
            Serial.Handshake = Handshake.RequestToSend;
        else
            Serial.Handshake = Handshake.None;

        if (!Serial.IsOpen)
            Serial.Open();

        try
        {
            byte[] TemperatureRequest = { 0x55, 0xAA, 0x8F };
            ComPortControll.WritePacket(Serial, TemperatureRequest);
            byte[] TemperatureResponse = ComPortControll.ReadPacket(Serial);

            for (int i = 0; i < TemperatureResponse.Length; i++)
            {
                Console.Write("{0:X} ", TemperatureResponse[i]);
            }
            Console.WriteLine();

            byte[] SerialNumberRequest = { 0x55, 0xAA, 0x5F };
            ComPortControll.WritePacket(Serial, SerialNumberRequest);
            byte[] SerialNumberResponse = ComPortControll.ReadPacket(Serial);
            byte[] SerialData = SerialNumberResponse.Skip(3).Take(4).ToArray();

            for (int i = 0; i < SerialNumberResponse.Length; i++)
            {
                Console.Write("{0:X} ", SerialNumberResponse[i]);
            }
            Console.WriteLine();

            for (int i = 0; i < SerialData.Length; i++)
            {
                Console.Write("{0:X} ", SerialData[i]);
            }
            Console.WriteLine();

            int SerialNumber = Convert.ToInt32(SerialData[1] << 24 | SerialData[0] << 16 | SerialData[2] << 8 | SerialData[3]);
            Console.WriteLine("Dozimeter SerialNumber = " + SerialNumber);

            byte[] PedRequest = { 0x55, 0xAA, 0x0F };
            ComPortControll.WritePacket(Serial, PedRequest);
            byte[] PedResponse = ComPortControll.ReadPacket(Serial);

            for (int i = 0; i < PedResponse.Length; i++)
            {
                Console.Write("{0:X} ", PedResponse[i]);
            }
            Console.WriteLine();

            //if (CheckSum.CheckXorCRC(PedReponse))
            {
                byte[] PedData = PedResponse.Skip(3).Take(4).ToArray();
                byte PedStaticError = PedResponse.Skip(7).Take(1).ToArray()[0];

                short IntPart = Convert.ToInt16(PedData[3] << 8 | PedData[2]);
                String floatPart = Convert.ToString(Convert.ToInt16(PedData[1] << 8 | PedData[0]));
                double pedValue = Convert.ToDouble(Convert.ToString(IntPart) + "." + floatPart);

                Console.WriteLine("PED = " + pedValue);
                Data.Add(new Parameters(Convert.ToString(this.SerialNumber), 131, 0, pedValue, 62));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(TAG + " " + ex.Message);
        }
        finally
        {
            Serial.Close();
        }
    }

    public List<Parameters> GetDataList()
    {
        if (Data.Any())
        {
            List<Parameters> Temp = new List<Parameters>(Data);
            Data.Clear();
            return Temp;
        }

        return new List<Parameters>();
    }
}

/// <summary>
/// This class is used for reading parameters from Ergomera-125
/// </summary>
public class Ergo125
{
    public class ReadRegister
    {
        public int register = 0;
        public int count = 0;

        public ReadRegister(int register, int count)
        {
            this.register = register;
            this.count = count;
        }
    }

    public byte[] readArrayTemplate = { 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    public int settingsLength = 513, ValuessLength = 451, archivesValuesLength = 132;
    public List<List<Parameters>> archives = new List<List<Parameters>>();
    public List<ReadRegister> registers = new List<ReadRegister>();
    public GPIO directionGpio = Program.Export("21", "out");
    public List<Parameters> current = new List<Parameters>();
    public String serialNumber = String.Empty;
    public String SerialNumber = String.Empty;
    public String rsType = String.Empty;
    public String mode = String.Empty;
    public int setFrequency = 0;
    public int baudRate = 0;
    public bool inversion;
    private int EquipmentTypeID = 51;

    /// <summary>
    /// Class construct
    /// </summary>
    /// <param name="SerialNuber">Ergomeer Serial Number</param>
    /// <param name="mode">Using for setting protocol<para/>
    /// ergo - Using ergo protocol <para/>
    /// modbus - Using for modbus protocol
    /// </param>
    /// <param name="baudRate">Com port speed</param>
    /// <param name="rsType">Type or RS connection <para/>
    /// 232 - using RS232 <para/>
    /// 485 - using RS485
    /// </param>
    /// <param name="setFrequency">Frequency of reading interval</param>
    public Ergo125(String SerialNuber, String mode, String baudRate, String rsType, String setFrequency, bool inversion)
    {
        this.serialNumber = SerialNuber.ToString();
        this.mode = mode;
        this.baudRate = Convert.ToInt32(baudRate);
        this.rsType = rsType;
        this.inversion = inversion;
        this.setFrequency = Convert.ToInt32(setFrequency);
        int id = Convert.ToInt32(SerialNuber) % 256;
        readArrayTemplate[0] = (byte)(id);
        CRC16(readArrayTemplate, readArrayTemplate.Length);
        AddCurrentToRegisters();
    }

    /// <summary>
    /// Using for reading current values from Ergomeers by Modbus protocol
    /// </summary>
    /// <param name="baudRate">Com port speed</param>
    public void ReadValuessModbus(int baudRate)
    {
        SerialPort port = new SerialPort("/dev/serial0", baudRate, Parity.None, 8, StopBits.One);
        List<byte> bytes = new List<byte>();

        if (this.rsType.Contains("232"))
            port.Handshake = Handshake.RequestToSend;
        else
            port.Handshake = Handshake.None;

        port.ReadTimeout = 10000;
        port.WriteTimeout = 10000;

        current.Clear();
        bytes.Clear();

        try
        {
            if (!(port.IsOpen)) port.Open();
            else port.DiscardOutBuffer();

            foreach (var item in registers)
            {
                readArrayTemplate[2] = (byte)((item.register >> 8));
                readArrayTemplate[3] = (byte)(item.register & 0xff);
                readArrayTemplate[4] = (byte)(item.count >> 8);
                readArrayTemplate[5] = (byte)(item.count & 0xff);
                CRC16(readArrayTemplate, readArrayTemplate.Length);

                //DirectionPinToOne();
                WritePacket(port, readArrayTemplate);

                //DirectionPinToZero();
                byte[] receive = ReadPacket(port, readArrayTemplate);

                if (receive.Length - 5 <= 0)
                {
                    throw new Exception("ReadError");
                }
                else Console.WriteLine("\n>>>   Need read - " + item.count + " readed " + (receive.Length - 5));

                for (int i = 3; i < receive.Length - 2; i++)
                {
                    Console.Write("{0:X} ", receive[i]);
                    bytes.Add(receive[i]);
                }
                Console.WriteLine();
            }

            current = ConvertBufferDataModbus(bytes, this.serialNumber);
            if (port.IsOpen) port.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message);
            if (port.IsOpen) port.Close();
            return;
        }
    }

    /// <summary>
    /// Using for reading current values from Ergomeer by Ergo protocol
    /// </summary>
    /// <param name="baudRate">Com port speed</param>
    /// <param name="inversion">Using for several Ergomeer versions</param>
    public void ReadValuesErgo(int baudRate)
    {
        List<byte> bytes = new List<byte>();
        SerialPort port = null;

        try
        {
            port = new SerialPort("/dev/serial0", baudRate, Parity.None, 8, StopBits.One); ;
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;

            if (this.rsType.Contains("232"))
                port.Handshake = Handshake.RequestToSend;
            else
                port.Handshake = Handshake.None;

            //if (this.rsType.Contains("232"))
            //{
            //    port = new SerialPort("/dev/rs232", baudRate, Parity.None, 8, StopBits.One);
            //    port.Handshake = Handshake.RequestToSend;
            //}
            //else
            //{
            //    port = new SerialPort("/dev/rs485", baudRate, Parity.None, 8, StopBits.One);
            //    port.Handshake = Handshake.None;
            //}

            current.Clear();
            bytes.Clear();

            if (!(port.IsOpen)) port.Open();
            port.DiscardOutBuffer();
            port.DiscardInBuffer();

            String request = "EMC !125m" + this.serialNumber + Convert.ToChar(0xd);

            //DirectionPinToOne();
            WritePacket(port, request);

            //DirectionPinToZero();
            byte[] receive = ReadPacket(port, request);
            port.Close();

            if (!(CheckCRC(receive, ValuessLength)))
            {
                Console.WriteLine("Read error");
                if (port.IsOpen) port.Close();
                return;
            }
            else
            {
                for (int i = 0; i < receive.Length; i++)
                {
                    Console.Write("{0:X} ", receive[i]);
                }

                current.Add(new Parameters(this.SerialNumber, 111, 0, Convert.ToDouble(this.SerialNumber), EquipmentTypeID));

                Console.WriteLine("\n>>>   Need read 451 bytes -  readed " + receive.Length);

                ConvertBufferDataErgo(receive.ToList(), this.serialNumber);

                if (port.IsOpen)
                    port.Close();
            }
            return;
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message);
            if (port != null && port.IsOpen)
                port.Close();
        }
    }

    /// <summary>
    /// Using for reading hourly archives for several date
    /// </summary>
    /// <param name="baudRate">Com port Speed</param>
    /// <param name="dateOfRead">The date for which you want to read the archive</param>
    /// <param name="inversion">Using for different Ergomeer versions</param>
    /// <returns></returns>
    public bool ReadArchivesValuesErgo(int baudRate, DateTime dateOfRead)
    {
        SerialPort port = new SerialPort("/dev/serial0", baudRate, Parity.None, 8, StopBits.One); ;
        List<byte> bytes = new List<byte>();

        if (this.rsType.Contains("232"))
            port.Handshake = Handshake.RequestToSend;
        else
            port.Handshake = Handshake.None;

        port.ReadTimeout = 5000;
        port.WriteTimeout = 5000;

        archives.Clear();
        bytes.Clear();

        //if (this.rsType.Contains("232"))
        //{
        //    port = new SerialPort("/dev/rs232", baudRate, Parity.None, 8, StopBits.One);
        //    port.Handshake = Handshake.RequestToSend;
        //}
        //else
        //{
        //    port = new SerialPort("/dev/rs485", baudRate, Parity.None, 8, StopBits.One);
        //    port.Handshake = Handshake.None;
        //}

        try
        {
            if (!(port.IsOpen)) port.Open();
            port.DiscardOutBuffer();
            port.DiscardInBuffer();

            int day = dateOfRead.Day;
            int month = dateOfRead.Month;
            int year = dateOfRead.Year - 2000;

            String dayStr = (day < 10) ? "0" + day : day.ToString();
            String monthStr = (month < 10) ? "0" + month : month.ToString();
            String yearStr = (year < 10) ? "0" + year : year.ToString();

            Console.WriteLine(">>> Trying to read Archive for date " + dateOfRead.ToString());

            String archiveDayRequest = "EMA !125m" + this.serialNumber.ToString().Trim() + " %0 " + dayStr + "." + monthStr + "." + yearStr + Convert.ToChar(0xd);
            Console.WriteLine(">>>   Trying to send request : " + archiveDayRequest);

            WritePacket(port, archiveDayRequest);
            Thread.Sleep(2000);

            byte[] receive = ReadPacket(port, archiveDayRequest);

            if (!(CheckCRC(receive, archivesValuesLength)))
            {
                Console.WriteLine("Read error");
                if (port.IsOpen) port.Close();
                return false;
            }
            else
            {
                for (int i = 0; i < receive.Length; i++)
                {
                    Console.Write("{0:X} ", receive[i]);
                }

                Console.WriteLine("\n>>>   Need read 131 bytes -  readed " + receive.Length);
                Thread.Sleep(2000);

                archives.Clear();
                bool ReadStatus = false;

                for (int j = 0; j < 24; j++)
                {
                    Console.WriteLine(">>> Trying to read Archive for hour : " + j);

                    List<Parameters> getData = new List<Parameters>();
                    ReadStatus = readArchiveString(port, "EMF !125m" + this.serialNumber + Convert.ToChar(0xd), this.serialNumber.ToString(), ref getData);

                    if (!(ReadStatus))
                    {
                        Console.WriteLine(">>>   Archive read failed");
                        if (port.IsOpen) port.Close();
                        return false;
                    }
                    else
                    {
                        Console.WriteLine(">>>   Archive read succesfully");
                        archives.Add(getData);
                    }
                }

                if (port.IsOpen)
                    port.Close();
            }
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message);
            if (port.IsOpen)
                port.Close();
            return false;
        }
    }

    /// <summary>
    /// Read next archive string
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="request">String or request to Ergomeer</param>
    /// <param name="serial">Ergomeer Serial Number</param>
    /// <param name="DataArchives">Date or reading archiv string</param>
    /// <param name="inversion">Used for different Ergomeer versions</param>
    /// <returns></returns>
    public bool readArchiveString(SerialPort port, String request, String serial, ref List<Parameters> DataArchives)
    {
        byte[] receive = new byte[65535];

        port.Write(request);
        Console.WriteLine(">>>   Trying to make request : " + request);
        Thread.Sleep(2000);

        int bytesRead = port.Read(receive, 0, receive.Length);
        Console.WriteLine(">>>   Read archives valuesLength " + bytesRead);
        receive = receive.Take(bytesRead).ToArray();

        if (!(CheckCRC(receive, archivesValuesLength)))
        {
            Console.WriteLine("Read error");
            return false;
        }
        else
        {
            for (int i = 0; i < receive.Length; i++)
            {
                Console.Write("{0:X} ", receive[i]);
            }

            ConvertArchiveDataErgo(receive.ToList(), serial, ref DataArchives);
            return true;
        }
    }

    /// <summary>
    /// Converting received bytes of archive values to list of values By Ergo protocol
    /// </summary>
    /// <param name="data">Byte array of responce</param>
    /// <param name="serial">Serial number of Ergomeer device</param>
    /// <param name="getData">List to store values</param>
    /// <param name="inversion">Used for different Ergommer versions</param>
    /// <returns>List of Values readed from Ergomeer</returns>
    public List<Parameters> ConvertArchiveDataErgo(List<byte> data, String serial, ref List<Parameters> getData)
    {
        try
        {
            getData.Clear();

            byte[] timeArray = data.Take(6).ToArray();
            int seconds = timeArray[0];
            int minutes = timeArray[1];
            int hours = timeArray[2];
            int days = timeArray[3];
            int month = timeArray[4];
            int year = timeArray[5] + 2000;

            DateTime archiveTime;

            try
            {
                archiveTime = new DateTime(year, month, days, hours, minutes, seconds);
            }
            catch (Exception ex)
            {
                archiveTime = new DateTime(year, month, days, 0, 0, 0);
            }

            byte[] heatArray = data.Skip(6).Take(8).ToArray();
            byte[] volumeArray = data.Skip(14).Take(16).ToArray();
            byte[] weightArray = data.Skip(30).Take(16).ToArray();
            byte[] time = data.Skip(46).Take(60).ToArray();
            byte[] temperature = data.Skip(106).Take(12).ToArray();
            byte[] pressure = data.Skip(108).Take(12).ToArray();

            if (this.inversion)
            {
                getData.Add(new Parameters(serial, 13, 0, Convert.ToDouble(BitConverter.ToSingle(heatArray.Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 13, 1, Convert.ToDouble(BitConverter.ToSingle(heatArray.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 91, 0, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 1, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(4).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 2, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(8).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 3, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(12).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 90, 0, Convert.ToDouble(BitConverter.ToSingle(weightArray.Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 1, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(4).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 2, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(8).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 3, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(12).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 1, 1, Convert.ToDouble(BitConverter.ToSingle(pressure.Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 1, 2, Convert.ToDouble(BitConverter.ToSingle(pressure.Skip(4).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 1, 3, Convert.ToDouble(BitConverter.ToSingle(pressure.Skip(8).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 95, 0, Convert.ToDouble(BitConverter.ToSingle(temperature.Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 95, 1, Convert.ToDouble(BitConverter.ToSingle(temperature.Skip(4).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 95, 2, Convert.ToDouble(BitConverter.ToSingle(temperature.Skip(8).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID, archiveTime));
            }
            else
            {
                getData.Add(new Parameters(serial, 13, 0, Convert.ToDouble(BitConverter.ToSingle(heatArray.Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 13, 1, Convert.ToDouble(BitConverter.ToSingle(heatArray.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 91, 0, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 1, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 2, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(8).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 91, 3, Convert.ToDouble(BitConverter.ToSingle(volumeArray.Skip(12).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 90, 0, Convert.ToDouble(BitConverter.ToSingle(weightArray.Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 1, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 2, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(8).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 90, 3, Convert.ToDouble(BitConverter.ToSingle(weightArray.Skip(12).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 1, 1, Convert.ToDouble(BitConverter.ToSingle(pressure.Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 1, 2, Convert.ToDouble(BitConverter.ToSingle(pressure.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 1, 3, Convert.ToDouble(BitConverter.ToSingle(pressure.Skip(8).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));

                getData.Add(new Parameters(serial, 95, 0, Convert.ToDouble(BitConverter.ToSingle(temperature.Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 95, 1, Convert.ToDouble(BitConverter.ToSingle(temperature.Skip(4).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
                getData.Add(new Parameters(serial, 95, 2, Convert.ToDouble(BitConverter.ToSingle(temperature.Skip(8).Take(4).ToArray(), 0)), EquipmentTypeID, archiveTime));
            }

            #region parse
            int obisCode = 0;
            int parameterNumber = 0;

            for (int i = 0; i < time.Length; i += 4)
            {
                if (i < 16)
                {
                    obisCode = 10;
                    if (i < 4) parameterNumber = 0;
                    else if (i < 8) parameterNumber = 1;
                    else if (i < 12) parameterNumber = 2;
                    else if (i < 16) parameterNumber = 3;
                }
                else if (i < 32)
                {
                    obisCode = 77;
                    if (i < 20) parameterNumber = 0;
                    else if (i < 24) parameterNumber = 1;
                    else if (i < 28) parameterNumber = 2;
                    else if (i < 32) parameterNumber = 3;
                }
                else if (i < 40)
                {
                    obisCode = 10;
                    if (i < 36) parameterNumber = 4;
                    else if (i < 40) parameterNumber = 5;
                }
                else if (i < 48)
                {
                    obisCode = 10;
                    if (i < 40) parameterNumber = 10;
                    else if (i < 48) parameterNumber = 11;
                }
                else if (i < 56)
                {
                    obisCode = 10;
                    if (i < 52) parameterNumber = 8;
                    else if (i < 56) parameterNumber = 9;
                }
                else if (i < 60)
                {
                    obisCode = 77;
                    parameterNumber = 4;
                }

                byte[] datas = time.Skip(i).Take(4).ToArray();
                for (int j = 0; j < datas.Length; j++)
                    Console.Write("{0:X} ", datas[j]);

                Console.Write(">>>   Time Array obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

                int ival = BitConverter.ToInt32(datas.Take(4).ToArray(), 0);

                getData.Add(new Parameters(serial.ToString(), obisCode, parameterNumber, Convert.ToDouble(ival), EquipmentTypeID, archiveTime));

                Console.Write(">>>   ==>> " + (ival));
                Console.WriteLine();
            }
            #endregion
            return getData;
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " ConvertArchivesErgo()");
            return current;
        }
    }

    /// <summary>
    /// Converting received bytes to list of values By Ergo protocol. Parsing data.
    /// </summary>
    /// <param name="data">Byte array of responce</param>
    /// <param name="serial">Serial number of Ergomeer device</param>
    /// <returns></returns>
    public List<Parameters> ConvertBufferDataErgo(List<byte> data, String serial)
    {
        if (data.Count < 450) return current;

        try
        {
            int obisCode = 0;
            int parameterNumber = 0;

            List<byte> first = data.Skip(16).Take(104).ToList();

            for (int i = 0; i < first.Count; i += 4)
            {
                if (i < 8)
                {
                    obisCode = 12;
                    if (i < 4) parameterNumber = 0;
                    else parameterNumber = 1;
                }
                else if (i < 24)
                {
                    obisCode = 94;
                    if (i < 12) parameterNumber = 0;
                    else if (i < 16) parameterNumber = 1;
                    else if (i < 20) parameterNumber = 2;
                    else if (i < 24) parameterNumber = 3;
                }
                else if (i < 40)
                {
                    obisCode = 93;
                    if (i < 28) parameterNumber = 0;
                    else if (i < 32) parameterNumber = 1;
                    else if (i < 36) parameterNumber = 2;
                    else if (i < 40) parameterNumber = 3;
                }
                else if (i < 52)
                {
                    obisCode = 95;
                    if (i < 44) parameterNumber = 0;
                    else if (i < 48) parameterNumber = 1;
                    else if (i < 52) parameterNumber = 2;
                }
                else if (i < 64)
                {
                    obisCode = 1;
                    if (i < 56) parameterNumber = 1;
                    else if (i < 60) parameterNumber = 2;
                    else if (i < 64) parameterNumber = 3;
                }
                else if (i < 80)
                {
                    obisCode = 94;
                    if (i < 68) parameterNumber = 4;
                    else if (i < 72) parameterNumber = 5;
                    else if (i < 76) parameterNumber = 6;
                    else if (i < 80) parameterNumber = 7;
                }
                else if (i < 96)
                {
                    obisCode = 93;
                    if (i < 84) parameterNumber = 4;
                    else if (i < 88) parameterNumber = 5;
                    else if (i < 92) parameterNumber = 6;
                    else if (i < 96) parameterNumber = 7;
                }
                else if (i < 104)
                {
                    obisCode = 95;
                    if (i < 100) parameterNumber = 3;
                    else if (i < 104) parameterNumber = 4;
                }

                byte[] datas = first.Skip(i).Take(4).ToArray();
                for (int j = 0; j < datas.Length; j++)
                    Console.Write("{0:X} ", datas[j]);
                Console.Write(">>>  obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

                if (this.inversion)
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(BitConverter.ToSingle(first.Skip(i).Take(4).Reverse().ToArray(), 0)), EquipmentTypeID));
                else
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(BitConverter.ToSingle(first.Skip(i).Take(4).ToArray(), 0)), EquipmentTypeID));

                Console.Write(">>>    ==>> " + Convert.ToDouble(BitConverter.ToSingle(first.Skip(i).Take(4).ToArray(), 0)));
                Console.WriteLine();
            }

            byte[] intVal = data.Skip(256).Take(40).ToArray();
            byte[] floatVal = data.Skip(296).Take(40).ToArray();

            for (int i = 0; i < 40; i += 4)
            {
                if (i < 8)
                {
                    obisCode = 13;
                    if (i < 4) parameterNumber = 0;
                    else parameterNumber = 1;
                }
                else
                {
                    if (i < 24)
                    {
                        obisCode = 91;
                        if (i < 12) parameterNumber = 0;
                        else if (i < 16) parameterNumber = 1;
                        else if (i < 20) parameterNumber = 2;
                        else if (i < 24) parameterNumber = 3;
                    }
                    else
                    {
                        if (i < 40)
                        {
                            obisCode = 90;
                            if (i < 28) parameterNumber = 0;
                            else if (i < 32) parameterNumber = 1;
                            else if (i < 34) parameterNumber = 2;
                            else if (i < 40) parameterNumber = 3;
                        }
                    }
                }

                byte[] tmpData = intVal.Skip(i).Take(4).ToArray();
                for (int j = 0; j < tmpData.Length; j++)
                    Console.Write("{0:X} ", tmpData[j]);
                Console.Write("  int Arr obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);
                Console.WriteLine();

                tmpData = floatVal.Skip(i).Take(4).ToArray();
                for (int j = 0; j < tmpData.Length; j++)
                    Console.Write("{0:X} ", tmpData[j]);

                Console.Write(">>>  floatArr obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

                if (inversion)
                {
                    int ival = BitConverter.ToInt32(intVal.Skip(i).Take(4).Reverse().ToArray(), 0);
                    double dval = Convert.ToDouble(BitConverter.ToSingle(floatVal.Skip(i).Take(4).Reverse().ToArray(), 0));
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(ival) + dval, EquipmentTypeID));
                    Console.Write(">>>   ==>> " + (ival + dval));
                    Console.WriteLine();
                }
                else
                {
                    int ival = BitConverter.ToInt32(intVal.Skip(i).Take(4).ToArray(), 0);
                    double dval = Convert.ToDouble(BitConverter.ToSingle(floatVal.Skip(i).Take(4).ToArray(), 0));
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(ival) + dval, EquipmentTypeID));
                    Console.Write(">>>   ==>> " + (ival + dval));
                    Console.WriteLine();
                }
            }

            byte[] time = data.Skip(336).Take(60).ToArray();
            for (int i = 0; i < time.Length; i += 4)
            {
                if (i < 16)
                {
                    obisCode = 10;
                    if (i < 4) parameterNumber = 0;
                    else if (i < 8) parameterNumber = 1;
                    else if (i < 12) parameterNumber = 2;
                    else if (i < 16) parameterNumber = 3;
                }
                else if (i < 32)
                {
                    obisCode = 77;
                    if (i < 20) parameterNumber = 0;
                    else if (i < 24) parameterNumber = 1;
                    else if (i < 28) parameterNumber = 2;
                    else if (i < 32) parameterNumber = 3;
                }
                else if (i < 40)
                {
                    obisCode = 10;
                    if (i < 36) parameterNumber = 4;
                    else if (i < 40) parameterNumber = 5;
                }
                else if (i < 48)
                {
                    obisCode = 10;
                    if (i < 40) parameterNumber = 10;
                    else if (i < 48) parameterNumber = 11;
                }
                else if (i < 56)
                {
                    obisCode = 10;
                    if (i < 52) parameterNumber = 8;
                    else if (i < 56) parameterNumber = 9;
                }
                else if (i < 60)
                {
                    obisCode = 77;
                    parameterNumber = 4;
                }

                byte[] datas = time.Skip(i).Take(4).ToArray();
                for (int j = 0; j < datas.Length; j++)
                    Console.Write("{0:X} ", datas[j]);

                Console.Write(">>>   Time Array obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

                if (inversion)
                {
                    int ival = BitConverter.ToInt32(datas.Take(4).Reverse().ToArray(), 0);
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(ival), EquipmentTypeID));
                    Console.Write(">>>   ==>> " + (ival));
                    Console.WriteLine();
                }
                else
                {
                    int ival = BitConverter.ToInt32(datas.Take(4).ToArray(), 0);
                    current.Add(new Parameters(this.SerialNumber, obisCode, parameterNumber, Convert.ToDouble(ival), EquipmentTypeID));
                    Console.Write(">>>   ==>> " + (ival));
                    Console.WriteLine();
                }
            }

            return current;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return current;
        }
    }

    /// <summary>
    /// Converting received bytes to list of values by Modbus protocol
    /// </summary>
    /// <param name="data">Byte array of responce</param>
    /// <param name="serial">Serial number of Ergomeer device</param>
    /// <returns></returns>
    public List<Parameters> ConvertBufferDataModbus(List<byte> data, String serial)
    {
        if (data.Count < 230) return current;

        try
        {
            int parameterNumber = 1;
            byte[] dt = new byte[4];

            byte[] massiveQuantity = new byte[4];
            for (int i = 0; i < 16; i += 4)
            {
                dt = data.Skip(i).Take(4).ToArray();
                for (int j = 0; j < dt.Length; j++)
                    Console.Write("{0:X} ", dt[j]);
                Console.Write(">>>  obisCode --> " + 93 + " parameterNumber --> " + (i / 4));

                massiveQuantity = data.Skip(i).Take(4).ToArray();
                current.Add(new Parameters(this.SerialNumber, 93, parameterNumber++, FloatConvert(massiveQuantity), EquipmentTypeID));

                Console.Write(">>>   ==>> " + FloatConvert(massiveQuantity));
                Console.WriteLine();
            }

            data = data.Skip(16).ToList();
            parameterNumber = 0;
            byte[] weightInt = data.Take(16).ToArray();
            byte[] weightFloat = data.Skip(16).Take(16).ToArray();

            for (int i = 0; i < 16; i += 4)
            {
                dt = weightInt.Skip(i).Take(4).ToArray();
                for (int j = 0; j < dt.Length; j++)
                    Console.Write("{0:X} ", dt[j]);
                Console.Write(">>>   integer Array   obisCode --> " + 90 + " parameterNumber --> " + (i / 4));
                Console.WriteLine();

                dt = weightFloat.Skip(i).Take(4).ToArray();
                for (int j = 0; j < dt.Length; j++)
                    Console.Write("{0:X} ", dt[j]);
                Console.Write(">>>   float Array   obisCode --> " + 90 + " parameterNumber --> " + (i / 4));

                int ival = IntConvert(weightInt.Skip(i).Take(4).ToArray());
                double dval = FloatConvert(weightFloat.Skip(i).Take(4).ToArray());
                current.Add(new Parameters(this.SerialNumber, 90, parameterNumber++, ival + dval, EquipmentTypeID));

                Console.Write(">>>   ==>> " + (ival + dval));
                Console.WriteLine();
            }

            data = data.Skip(32).ToList();
            byte[] intArr = data.Take(24).ToArray();
            byte[] floatArr = data.Skip(24).Take(24).ToArray();

            for (int i = 0, j = 0; i < 24; i += 4)
            {
                dt = intArr.Skip(i).Take(4).ToArray();
                for (int k = 0; k < dt.Length; k++)
                    Console.Write("{0:X} ", dt[k]);
                if (i < 8)
                    Console.Write(">>>   int Array   obisCode --> " + 13 + " parameterNumber --> " + (i / 4));
                else
                    Console.Write(">>>   int Array   obisCode --> " + 91 + " parameterNumber --> " + (j / 4));

                Console.WriteLine();

                dt = floatArr.Skip(i).Take(4).ToArray();
                for (int k = 0; k < dt.Length; k++)
                    Console.Write("{0:X} ", dt[k]);

                if (i < 8)
                    Console.Write(">>>   float Array   obisCode --> " + 13 + " parameterNumber --> " + (i / 4));
                else
                    Console.Write(">>>   float Array   obisCode --> " + 91 + " parameterNumber --> " + (j / 4));

                int ival = IntConvert(intArr.Skip(i).Take(4).ToArray());
                double dval = FloatConvert(floatArr.Skip(i).Take(4).ToArray());

                if (i < 8)
                {
                    current.Add(new Parameters(serial, 13, ((i + 1) / 4), ival + dval, EquipmentTypeID));
                }
                else
                {
                    current.Add(new Parameters(this.SerialNumber, 91, ((j + 1) / 4), ival + dval, EquipmentTypeID));
                    j += 4;
                }
                Console.Write(">>>   ==>> " + (ival + dval));
                Console.WriteLine();
            }

            data = data.Skip(48).ToList();
            byte[] time = data.Skip(0).Take(4).ToArray();
            byte[] date = data.Skip(4).Take(4).ToArray();

            byte[] heatPower = data.Skip(8).Take(8).ToArray();
            AddToCurrentFloat(heatPower, this.SerialNumber, 12, 0, 2);

            byte[] volumeQuantity = data.Skip(16).Take(24).ToArray();
            AddToCurrentFloat(volumeQuantity, this.SerialNumber, 94, 0, 6);

            byte[] temperatute = data.Skip(40).Take(20).ToArray();
            AddToCurrentFloat(temperatute, this.SerialNumber, 95, 0, 5);

            byte[] pressure = data.Skip(60).Take(12).ToArray();
            AddToCurrentFloat(pressure, this.SerialNumber, 1, 0, 3);

            byte[] workingTime = data.Skip(72).Take(16).ToArray();
            AddToCurrentLong(workingTime, this.SerialNumber, 10, 0, 4);

            byte[] nonWorkingTime = data.Skip(88).Take(16).ToArray();
            AddToCurrentLong(nonWorkingTime, this.SerialNumber, 77, 0, 4);

            byte[] specificWorkingTime = data.Skip(104).Take(32).ToArray();
            AddToCurrentLong(specificWorkingTime, this.SerialNumber, 10, 4, 8);

            byte[] nonWorkingTimeAll = data.Skip(136).ToArray();
            AddToCurrentLong(nonWorkingTimeAll, this.SerialNumber, 77, 4, 1);

            return current;
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message);
            return current;
        }
    }

    /// <summary>
    /// Check Modbus CRC from received response
    /// </summary>
    /// <param name="data">Byte array that confirm value<para/></param>
    /// <param name="size">Size of request array</param>
    /// <returns>Return result of matching CRC for response</returns>
    public bool CheckCRC(byte[] array, int neededLength)
    {
        if (array.Length < neededLength)
            return false;

        if (array.Length > neededLength)
            array = array.Take(neededLength).ToArray();

        byte check = 0xA5;

        for (int i = 0; i < neededLength - 1; i++)
        {
            check ^= array[i];
        }

        if (check == array[neededLength - 1])
            return true;
        else return false;
    }
    
    /// <summary>
    /// Read serial number of Ergomeer device
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <returns></returns>
    public String ReadSerialNumber(SerialPort port)
    {
        readArrayTemplate = new byte[] { readArrayTemplate[0], 0x03, 0x00, 0xBA, 0x00, 0x01, 0x00, 0x00 };
        CRC16(readArrayTemplate, readArrayTemplate.Length);

        WritePacket(port, readArrayTemplate);
        byte[] receive = ReadPacket(port, readArrayTemplate);

        String identfier = Convert.ToString(BitConverter.ToInt16(receive, 0));
        return identfier;
    }

    /// <summary>
    ///Convert value to float and add it to current List 
    /// </summary>
    public void AddToCurrentFloat(byte[] data, String serial, int obisCode, int parameterNumber, int count)
    {
        for (int i = 0; i < count * 4; i += 4)
        {
            byte[] dt = data.Skip(i).Take(4).ToArray();
            for (int j = 0; j < dt.Length; j++)
                Console.Write("{0:X} ", dt[j]);
            Console.Write("  obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

            byte[] value = data.Skip(i).Take(4).ToArray();
            double val = FloatConvert(value);
            current.Add(new Parameters(SerialNumber, obisCode, parameterNumber++, val, EquipmentTypeID));

            Console.Write("   ==>> " + val);
            Console.WriteLine();
        }
    }

    /// <summary>
    ///Convert value to long and add it to current List 
    /// </summary>
    public void AddToCurrentLong(byte[] data, String serial, int obisCode, int parameterNumber, int count)
    {
        for (int i = 0; i < count * 4; i += 4)
        {
            byte[] dt = data.Skip(i).Take(4).ToArray();
            for (int j = 0; j < dt.Length; j++)
                Console.Write("{0:X} ", dt[j]);
            Console.Write("  obisCode --> " + obisCode + " parameterNumber --> " + parameterNumber);

            byte[] value = data.Skip(i).Take(4).ToArray();
            double val = IntConvert(value);
            current.Add(new Parameters(SerialNumber, obisCode, parameterNumber++, val, EquipmentTypeID));

            Console.Write("   ==>> " + val);
            Console.WriteLine();
        }
    }

    /// <summary>
    /// Initialize Modbus registers for reading
    /// </summary>
    private void AddCurrentToRegisters()
    {
        /// 118 registers == 236 bytes
        registers.Add(new ReadRegister(0x03F0, 8)); //41009 - read 8 registers

        registers.Add(new ReadRegister(0x0410, 8)); //41041 - read 8 registers
        registers.Add(new ReadRegister(0x0794, 8)); //41941 - read 8 registers

        registers.Add(new ReadRegister(0x0024, 12));//40037 - read 12 registers
        registers.Add(new ReadRegister(0x03A8, 12));// -read 12 registers

        registers.Add(new ReadRegister(0x0000, 36));//40001 - read 36 registers
        registers.Add(new ReadRegister(0x0030, 34));//40049 - read 8 registers
    }

    /// <summary>
    /// Convert byte array to float value
    /// </summary>
    /// <param name="data">Byte array that confirm value</param>
    /// <returns>Converted float value</returns>
    private Double FloatConvert(byte[] data)
    {
        byte[] newValue = new byte[4];
        newValue[0] = data[1];
        newValue[1] = data[0];
        newValue[2] = data[3];
        newValue[3] = data[2];
        return Convert.ToDouble(BitConverter.ToSingle(newValue, 0));
    }

    /// <summary>
    /// Convert byte array to int value
    /// </summary>
    /// <param name="data">Byte array that confirm value</param>
    /// <returns>Converted int value</returns>
    private int IntConvert(byte[] data)
    {
        byte[] newValue = new byte[4];
        newValue[0] = data[1];
        newValue[1] = data[0];
        newValue[2] = data[3];
        newValue[3] = data[2];
        return BitConverter.ToInt32(newValue, 0);
    }

    /// <summary>
    /// Calculating Modbus CRC16 for request
    /// </summary>
    /// <param name="data">Byte array that confirm value<para/></param>
    /// <param name="size">Size of request array</param>
    /// <returns>Requet with calculated CRC</returns>
    private short CRC16(byte[] data, int size)
    {
        int Sum = 0;
        int byte_cnt = size - 2;
        int shift_cnt;
        Sum = (ushort)0xffff;
        int i = 0;
        for (; byte_cnt > 0; byte_cnt--)
        {
            Sum = (ushort)((Sum / 256) * 256 + ((Sum % 256) ^ data[i++]));
            for (shift_cnt = 0; shift_cnt < 8; shift_cnt++)
            {
                if ((Sum & 0x1) == 1) Sum = ((Sum >> 1) ^ 0xa001);
                else Sum >>= 1;
            }
        }
        byte[] byteArray = BitConverter.GetBytes((short)Sum);
        data[data.Length - 2] = byteArray[0];
        data[data.Length - 1] = byteArray[1];
        return BitConverter.ToInt16(new byte[2] { data[data.Length - 2], data[data.Length - 1] }, 0);
    }

    /// <summary>
    /// Write packet to serial port
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="data">String Request to serial port</param>
    private void WritePacket(SerialPort port, String data)
    {
        port.Write(data);
    }


    /// <summary>
    /// Write packet to serial port
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="data">Byte Request to serial port</param>
    private void WritePacket(SerialPort port, byte[] data)
    {
        port.Write(data, 0, data.Length);
    }

    /// <summary>
    /// Read packet from serialPort
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="lastSend">Last sending string request</param>
    /// <returns>Byte of response</returns>
    public byte[] ReadPacket(SerialPort port, String lastSend)
    {
        int readBuffer = port.BytesToRead;
        byte[] receive = new byte[readBuffer];
        port.Read(receive, 0, readBuffer);
        return receive;
    }

    /// <summary>
    /// Read packet from serialPort
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="lastSend">Last sending byte request</param>
    public byte[] ReadPacket(SerialPort port, byte[] lastSend)
    {
        int readBuffer = port.BytesToRead;
        byte[] receive = new byte[readBuffer];
        port.Read(receive, 0, readBuffer);
        return receive;
    }

    /// <summary>
    /// Set direction pin to 1
    /// </summary>
    public void DirectionPinToOne()
    {
        directionGpio.GPIOSetValue("1");
    }

    /// <summary>
    /// Set direction pin to 0
    /// </summary>
    public void DirectionPinToZero()
    {
        directionGpio.GPIOSetValue("0");
    }
}

/// <summary>
/// NiK parametes struckt <para/>
/// Using to store readed values form NIK
/// </summary>
public struct NikStruct
{
    public string SumPower;
    public string ActPower;

    public string Voltage_1;
    public string Voltage_2;
    public string Voltage_3;

    public string Current_1;
    public string Current_2;
    public string Current_3;

    public string Power_1;
    public string Power_2;
    public string Power_3;

    public long WorkingTime;
}

/// <summary>
/// Class is used for reading current values from NIK
/// </summary>
public class NIK
{
    public List<Parameters> values = new List<Parameters>();
    public String RaspberryID = String.Empty;
    public DateTime previous = DateTime.Now;
    public int address = 0;
    public int EquipmentID = 15;
    public long totalTime = 0;
    public NikStruct nik;

    public static byte[] SNRM = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x93, 0x06, 0x13, 0x7e }; //frame SNRM
    public static byte[] AARQ = { 0x7e, 0xa0, 0x50, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x0d, 0x3e, 0xe6, 0xe6, 0x00,
                                      0x60, 0x3f ,0xa1, 0x09, 0x06, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x01, 0x01, 0x8a,
                                      0x02, 0x07, 0x80 ,0x8b ,0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x02, 0x01, 0xac, 0x12,
                                      0x80, 0x11, 0x00, 0x31, 0x31, 0x31 ,0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31,
                                      0x31, 0x31, 0x31, 0x31, 0x31, 0xbe, 0x10, 0x04 ,0x0e, 0x01, 0x00, 0x00, 0x00, 0x06,
                                      0x5f, 0x1f, 0x04, 0x00, 0x00, 0x08, 0xcb, 0x00, 0x80, 0x9c ,0x46, 0x7e }; // AARQ request for authorization by User with passord

    public static byte[] DISC = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x53, 0x0a, 0xd5, 0x7e }; // frame DISC  

    //Requests for Parameters

    public static byte[] readVoltage_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                               0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x20, 0x07, 0x00, 0x00, 0x02, 0x00, 0x73, 0x88, 0x7e };

    public static byte[] readVoltage_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x93,
                                               0x17, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x34, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6f, 0xda, 0x7e };

    public static byte[] readVoltage_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                               0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x48, 0x07, 0x00, 0x00, 0x02, 0x00, 0x9a, 0x2e, 0x7e };

    public static byte[] readAmper_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x1f, 0x07, 0x00, 0x00, 0x02, 0x00, 0x2a, 0x72, 0x7e, };

    public static byte[] readAmper_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x33, 0x07, 0x00, 0x00, 0x02, 0x00, 0xbe, 0xc6, 0x7e };

    public static byte[] readAmper_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x47, 0x07, 0x00, 0x00, 0x02, 0x00, 0x13, 0x13, 0x7e };

    public static byte[] readPower_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x15, 0x07, 0x00, 0x00, 0x02, 0x00, 0x84, 0xd6, 0x7e };

    public static byte[] readPower_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x29, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6d, 0x70, 0x7e };

    public static byte[] readPower_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                             0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x3D, 0x07, 0x00, 0x00, 0x02, 0x00, 0x24, 0x5b, 0x7e };

    public static byte[] AvrPow = { 0x7E, 0xA0, 0x1C, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48, 0xE6,
                                        0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01, 0x00, 0x01, 0x07, 0x00,
                                        0x00, 0x02, 0x00, 0x38, 0x09, 0x7E };

    public static byte[] SumPow = { 0x7E, 0xA0, 0x1C, 0x06, 0xA8, 0xAC, 0x01, 0x21, 0x10, 0x91, 0x48, 0xE6,
                                        0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01, 0x00, 0x01, 0x08, 0x00,
                                        0xFF, 0x02, 0x00, 0x37, 0xA5, 0x7E};

    /// <summary>
    /// NIK Class construct
    /// </summary>
    /// <param name="address">NIK device address</param>
    /// <param name="RaspberryID">RaspberryID stored in Configuration.xml file</param>
    public NIK(int address, String RaspberryID)
    {
        this.RaspberryID = RaspberryID;
        this.address = address;

        try
        {
            ushort high = 0, low = 0;
            BarcodeToAddr(address, ref high, ref low);
            high = HDLC_ADDRESS_CONVERT(high, 0);
            low = HDLC_ADDRESS_CONVERT(low, 1);

            address = (high << 16) | low;
            CalculateSum(address);
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function Nik()");
        }
    }

    private static void HDLCConvert(byte[] array)
    {
        byte[] sum = new byte[2];
        byte[] HDLC_HEADER = new byte[8];
        for (int i = 1, j = 0; i < HDLC_HEADER.Length + 1; i++, j++)
        {
            HDLC_HEADER[j] = array[i];
        }
        short crc = CrcHDLC(HDLC_HEADER, HDLC_HEADER.Length, 0);
        sum = BitConverter.GetBytes(crc);
        array[9] = sum[0];
        array[10] = sum[1];
    }

    private static void CalculateArray(byte[] array)
    {
        byte[] ret = new byte[2];
        int size = array.Length - 4;
        byte[] cal = new byte[size];
        int k = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0 && i < array.Length - 3)
            {
                cal[k] = array[i];
                k++;
            }
        }
        short crc = CrcHDLC(cal, cal.Length, 0);//calculate_CRC16(cal, cal.Length);
        ret = BitConverter.GetBytes(crc);
        Array.Reverse(ret);
        array[array.Length - 2] = ret[0];
        array[array.Length - 3] = ret[1];
    }

    /// <summary>
    /// Calcalate CRC for all requests to NIK
    /// </summary>
    /// <param name="address">NIK address</param>
    private static void CalculateSum(int address)
    {
        byte[] array = BitConverter.GetBytes(address);
        Array.Reverse(array);

        for (int i = 3, j = 0; i < 7; i++, j++)
        {
            SNRM[i] = array[j];
            AARQ[i] = array[j];
            DISC[i] = array[j];
            readVoltage_A[i] = array[j];
            readVoltage_B[i] = array[j];
            readVoltage_C[i] = array[j];
            readAmper_A[i] = array[j];
            readAmper_B[i] = array[j];
            readAmper_C[i] = array[j];
            readPower_A[i] = array[j];
            readPower_B[i] = array[j];
            readPower_C[i] = array[j];
            SumPow[i] = array[j];
            AvrPow[i] = array[j];
        }

        HDLCConvert(AARQ);
        HDLCConvert(readVoltage_A);
        HDLCConvert(readVoltage_B);
        HDLCConvert(readVoltage_C);
        HDLCConvert(readAmper_A);
        HDLCConvert(readAmper_B);
        HDLCConvert(readAmper_C);
        HDLCConvert(readPower_A);
        HDLCConvert(readPower_B);
        HDLCConvert(readPower_C);
        HDLCConvert(AvrPow);
        HDLCConvert(SumPow);
        CalculateArray(SNRM);
        CalculateArray(AARQ);
        CalculateArray(DISC);
        CalculateArray(readVoltage_A);
        CalculateArray(readVoltage_B);
        CalculateArray(readVoltage_C);
        CalculateArray(readAmper_A);
        CalculateArray(readAmper_B);
        CalculateArray(readAmper_C);
        CalculateArray(readPower_A);
        CalculateArray(readPower_B);
        CalculateArray(readPower_C);
        CalculateArray(AvrPow);
        CalculateArray(SumPow);
    }

    /// <summary>
    /// Using for calculate CRC of Array
    /// </summary>
    /// <param name="data">Byte array need to be calculated</param>
    /// <param name="size">Bite array size</param>
    /// <param name="iOffset">Offset of byte array</param>
    /// <returns></returns>
    public static short CrcHDLC(byte[] data, int size, int iOffset)
    {
        int wCrc;
        byte bTmp;

        wCrc = (int)0xFFFF;//InitHdlcCrc;
        for (int i = 0; i < size; i++)
        {
            bTmp = (byte)(data[iOffset + i] ^ (byte)(wCrc & 0x00ff));
            bTmp = (byte)((bTmp << 4) ^ bTmp);
            wCrc = (int)(wCrc >> 8);

            int iTmp0 = bTmp << 8;
            iTmp0 ^= (bTmp << 3) & 0x07ff;
            iTmp0 ^= (bTmp >> 4) & 0x0000000f;

            wCrc ^= iTmp0 & 0x0000ffff;
        }
        wCrc ^= 0xffff;

        return (short)wCrc;
    }

    /// <summary>
    /// Read packet from NIK
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <returns>Readed bytes</returns>
    public static byte[] ReadPacket(SerialPort port)
    {
        int byteCount = port.BytesToRead;
        byte[] retBytes = new byte[byteCount];
        port.Read(retBytes, 0, byteCount);
        return retBytes;
    }

    /// <summary>
    /// Using for reading current values from NIK and store it in list
    /// </summary>
    /// <returns>Return List of readed values from NIK</returns>
    public List<Parameters> ReadCurrent()
    {
        byte[] receive = new byte[64];

        try
        {
            SerialPort port = new SerialPort("/dev/serial0", 9600, Parity.None, 8, StopBits.One);

            port.Handshake = Handshake.None;
            port.ReadTimeout = 2000;
            port.WriteTimeout = 2000;
            values.Clear();

            if (!(port.IsOpen)) port.Open();

            port.Write(SNRM, 0, SNRM.Length); //Sending frame SNRM
            Thread.Sleep(1000);
            receive = ReadPacket(port); //Answer from server UA

            port.Write(AARQ, 0, AARQ.Length); //Sending frame AARQ
            Thread.Sleep(1000);
            receive = ReadPacket(port); //Answer from server AARE

            nik.Voltage_1 = GetParam(port, readVoltage_A, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 97, 0, Convert.ToDouble(nik.Voltage_1), 15));

            nik.Voltage_2 = GetParam(port, readVoltage_B, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 97, 1, Convert.ToDouble(nik.Voltage_2), 15));

            nik.Voltage_3 = GetParam(port, readVoltage_C, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 97, 2, Convert.ToDouble(nik.Voltage_3), EquipmentID));

            nik.Current_1 = GetParam(port, readAmper_A, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 3, 1, Convert.ToDouble(nik.Current_1), EquipmentID));

            nik.Current_2 = GetParam(port, readAmper_B, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 3, 2, Convert.ToDouble(nik.Current_2), EquipmentID));

            nik.Current_3 = GetParam(port, readAmper_C, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 3, 3, Convert.ToDouble(nik.Current_3), EquipmentID));

            nik.Power_1 = GetParam(port, readPower_A, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 2, 0, Convert.ToDouble(nik.Power_1), EquipmentID));

            nik.Power_2 = GetParam(port, readPower_B, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 2, 1, Convert.ToDouble(nik.Power_2), EquipmentID));

            nik.Power_3 = GetParam(port, readPower_C, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 2, 2, Convert.ToDouble(nik.Power_3), EquipmentID));

            nik.SumPower = GetParam(port, SumPow, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 42, 5, Convert.ToDouble(nik.SumPower), EquipmentID));

            nik.ActPower = GetParam(port, AvrPow, 20, 29, 1000);
            values.Add(new Parameters(address.ToString(), 85, 4, Convert.ToDouble(nik.ActPower), EquipmentID));

            port.Write(DISC, 0, DISC.Length); //Sending AARQ request
            Thread.Sleep(1000);

            receive = ReadPacket(port); // Receive succsess request
            if (port.IsOpen) port.Close();

            return new List<Parameters>();
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function Nik()");
            return new List<Parameters>();
        }
    }

    /// <summary>
    /// Using for reading one parameter from NIK
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <param name="request">Request to read</param>
    /// <param name="first">Start of value data byte in responce</param>
    /// <param name="last">End of value data byte in responce</param>
    /// <param name="delayTime">Delay before reading paket</param>
    /// <returns>Return string representation of readed value</returns>
    public static string GetParam(SerialPort port, byte[] request, int first, int last, int delayTime)
    {
        string g = String.Empty;

        try
        {
            byte[] receive = new byte[64];
            port.Write(request, 0, request.Length); //Sending frame DISC
            Thread.Sleep(delayTime);  //maybe 200

            receive = ReadPacket(port); // receive packet after request

            for (int i = first; i < last; i++)
            {
                g += Convert.ToChar(receive[i]);
            }
            Console.WriteLine(">>>   " + g);
            return g;
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function nik GetParam()");
            return String.Empty;
        }
    }

    private static void BarcodeToAddr(long bar, ref ushort addrHI, ref ushort addrLO)
    {
        addrHI = 0;
        addrLO = (ushort)(bar & 0x3FFF);
        if ((bar & 0x3FFF) < 16)
        {
            addrLO += 16;
            addrHI |= (1 << 12);
        }
        else
        {
            addrLO = (ushort)(bar & 0x3FFF);
        }
        if (((bar >> 14) & 0x3FFF) < 16)
        {
            addrHI += (ushort)(((bar >> 14) & 0x3FFF) + 16);
            addrHI |= (1 << 13);
        }
        else
        {
            addrHI |= (ushort)((bar >> 14) & 0x3FFF);
        }
    }

    private static ushort HDLC_ADDRESS_CONVERT(int a, int l)
    {
        return (ushort)(((((a) << 1) & 0x00FE) | (l)) | (((a) << 2) & 0xFE00));
    }

    private static ushort HDLC_ADDRESS_DECODE(int a)
    {
        return (ushort)((((a) >> 1) & 0x007F) | (((a) >> 2) & 0x3F80));
    }
}

/// <summary>
/// Class that is used to store parameters from Pumps in Irpen
/// </summary>
public class PumpParameters
{
    public int PumpNumber;
    public int PumpState;
    public double PumpNominalPower;
    public double coefficient;

    public double SumPower;
    public double ActPower;

    public string Voltage_1;
    public string Voltage_2;
    public string Voltage_3;

    public string Current_1;
    public string Current_2;
    public string Current_3;

    public string Power_1;
    public string Power_2;
    public string Power_3;
    public long WorkinTime;
    public DateTime lastReading;

    /// <summary>
    /// Class constructor
    /// </summary>
    public PumpParameters()
    {
        this.PumpNumber = 0;
        this.PumpNominalPower = 0;
        this.PumpState = 0;

        this.SumPower = 0;
        this.ActPower = 0;

        this.Voltage_1 = String.Empty;
        this.Voltage_2 = String.Empty;
        this.Voltage_3 = String.Empty;

        this.Current_1 = String.Empty;
        this.Current_2 = String.Empty;
        this.Current_3 = String.Empty;

        this.Power_1 = String.Empty;
        this.Power_2 = String.Empty;
        this.Power_3 = String.Empty;

        this.WorkinTime = 0;
    }
}

public class Program
{
    public static string key = "IIltpohall", Station = String.Empty, Server = String.Empty, Port = String.Empty, RaspberryID = String.Empty, EquipmentTypeID = String.Empty;
    private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");
    private static long ErrorWord = 0, counter = 0, LastCurrentState = 0, totalTime = 0;
    private static int ReleOneState = 0, ReleTwoState = 0, count = 0, errorCounter = 0;
    private static List<PumpParameters> parameters = new List<PumpParameters>();
    private static List<Parameters> RaspberryParameters = new List<Parameters>();
    private static List<DateTime> PumpDateTime = new List<DateTime>();
    private static List<Command> commandsList = new List<Command>();
    private static List<Thread> ThreadList = new List<Thread>();
    private static List<Ergo125> ergoList = new List<Ergo125>();
    private static List<String> RaspberryInfo = new List<string>();
    private static List<GPIO> GPIOPumps = new List<GPIO>();
    private static List<int> PumpsPower = new List<int>();
    private static object commandsLocker = new object();
    private static object serialLocker = new object();
    private static object errorLocker = new object();
    private static object sendLocker = new object();
    private static string starterPin = String.Empty;
    private static int keysize = 256;
    private static SerialPort port;
    public static GPIO gpioReleOne;
    public static GPIO gpioReleTwo;

    private static Dozor GlobalDozor;
    private static MeteoStation GlobalMeteoStation;

    /*  Error codes
        0 - No Problems
        1 - Problems with pulse counter
        2 - Problems with move sensor
        3 - Problems with starter
        4 - Problems with sending info to server
        5 - Problems with nik
        6 - Problems with power supply
        7 - Starter not start
        8 - Start rele not start
        9 - Stop rele not start
     */

    /// <summary>
    /// Using to encrypt string
    /// </summary>
    /// <param name="plainText">Text for encrypt</param>
    /// <param name="passPhrase">Key to encrypt</param>
    /// <returns></returns>
    public static string Encrypt(string plainText, string passPhrase)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
        {
            byte[] keyBytes = password.GetBytes(keysize / 8);
            using (RijndaelManaged symmetricKey = new RijndaelManaged())
            {
                symmetricKey.Mode = CipherMode.CBC;
                using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                            cryptoStream.FlushFinalBlock();
                            byte[] cipherTextBytes = memoryStream.ToArray();
                            return Convert.ToBase64String(cipherTextBytes);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Using to decrypt string
    /// </summary>
    /// <param name="cipherText">Text for decrypt</param>
    /// <param name="passPhrase">Key to decrypt</param>
    /// <returns></returns>
    public static string Decrypt(string cipherText, string passPhrase)
    {
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
        {
            byte[] keyBytes = password.GetBytes(keysize / 8);
            using (RijndaelManaged symmetricKey = new RijndaelManaged())
            {
                symmetricKey.Mode = CipherMode.CBC;
                using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                {
                    using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                            int decryptedByteCount = 0;

                            decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Using for parse Configuration file to List of Strings
    /// </summary>
    /// <param name="path">Path to Configuraton.xml path</param>
    /// <returns>List of all necessary string from Configuration.xml file</returns>
    static public List<string> ReadFileLineByLine(string path)
    {
        List<string> returnValue = new List<string>();

        try
        {
            String line = String.Empty;
            StreamReader file = new StreamReader(path);

            while ((line = file.ReadLine()) != null)
            {
                while (line != "<conf>")
                {
                    line = line.Replace("\'", "").Replace(" ", "");
                    Console.WriteLine(line);
                    if (line.ToLower().Contains("stationid")) Station = line.ToLower().Replace("stationid=", "");
                    if (line.ToLower().Contains("raspberryid")) RaspberryID = line.ToLower().Replace("raspberryid=", "");
                    if (line.ToLower().Contains("server")) Server = line.ToLower().Replace("server=", "");
                    if (line.ToLower().Contains("port")) Port = line.ToLower().Replace("port=", "");
                    if (line.ToLower().Contains("equipmenttypeid")) EquipmentTypeID = line.ToLower().Replace("equipmenttypeid=", "");
                    line = file.ReadLine().ToLower();
                }
                while ((line = file.ReadLine()) != "</conf>")
                {
                    Console.WriteLine(line);
                    returnValue.Add(line);
                }
            }
            file.Close();
            return returnValue;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message + " function ReadFileLineByLine()");
            return returnValue;
        }
    }

    /// <summary>
    /// Using to replace xml tags in Configuratoin file
    /// </summary>
    /// <param name="replace">Strings from Configuration.xml file with xml tags</param>
    /// <returns>Strings from Configuration.xml file withou xml tags</returns>
    private static List<string> ReplaceUnusedCharacters(List<string> replace)
    {
        for (int i = 0; i < replace.Count; i++)
        {
            replace[i] = replace[i].Replace("\'", "");
            replace[i] = replace[i].Replace("\t", "");
            replace[i] = replace[i].Replace("</", "");
            replace[i] = replace[i].Replace("<", "");
            replace[i] = replace[i].Replace(">", "");
            replace[i] = replace[i].Replace("\n", "");
            replace[i] = replace[i].Replace("\'", "");
            replace[i] = replace[i].Replace(" ", "");
            if (replace[i] == "") replace.RemoveAt(i);
        }
        return replace;
    }

    /// <summary>
    /// Read Packet from serial port
    /// </summary>
    /// <param name="port">Serial port</param>
    /// <returns>Bytes that are reading from serial port</returns>
    private static byte[] ReadPacket(SerialPort port)
    {
        int byteCount = port.BytesToRead;
        byte[] retBytes = new byte[byteCount];
        port.Read(retBytes, 0, byteCount);
        return retBytes;
    }

    public static void CalculatePumpsParameters(NikStruct nik)
    {
        try
        {
            double totalNominalPowerWorking = 0, totalNominalPower = 0;
            int pumpsCount = PumpsPower.Count, workingPumps = 0;

            for (int i = 0; i < PumpsPower.Count; i++)
            {
                parameters[i].PumpState = GPIOPumps[i].GPIOGetValue(); // get pump state

                if (parameters[i].PumpState == 0) parameters[i].PumpState = 1;
                else parameters[i].PumpState = 0;

                parameters[i].PumpNominalPower = PumpsPower[i]; // get pump nominal power
                parameters[i].PumpNumber = i + 1; // get pump number

                totalNominalPower = totalNominalPower + PumpsPower[i]; // find sum of nominal powers

                if (parameters[i].PumpState == 1)
                {
                    workingPumps = workingPumps + 1; // find count of pumps in work
                    totalNominalPowerWorking = totalNominalPowerWorking + PumpsPower[i]; // find sum of nominal powers of WORKING pumps
                }
                else // if pump is not active
                {
                    parameters[i].Power_1 = "0";
                    parameters[i].Power_2 = "0";
                    parameters[i].Power_3 = "0";
                    parameters[i].Current_1 = "0";
                    parameters[i].Current_2 = "0";
                    parameters[i].Current_3 = "0";
                    parameters[i].ActPower = 0;
                }
            }

            Console.WriteLine(">>>    Total nominal Power of working pumps : " + totalNominalPowerWorking);

            for (int i = 0; i < PumpsPower.Count; i++)
            {

                parameters[i].Voltage_1 = nik.Voltage_1;
                parameters[i].Voltage_2 = nik.Voltage_2;
                parameters[i].Voltage_3 = nik.Voltage_3;

                if (parameters[i].PumpState == 1)
                {
                    parameters[i].coefficient = parameters[i].PumpNominalPower / totalNominalPowerWorking;
                    Console.WriteLine(">>>   Coefficient = " + parameters[i].coefficient);
                    parameters[i].ActPower = parameters[i].coefficient * Convert.ToDouble(nik.ActPower) / 1000;
                    parameters[i].Current_1 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_1) / 1000);
                    parameters[i].Current_2 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_2) / 1000);
                    parameters[i].Current_3 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_3) / 1000);
                    parameters[i].Power_1 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_1) / 1000);
                    parameters[i].Power_2 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_2) / 1000);
                    parameters[i].Power_3 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_3) / 1000);
                }
                Console.WriteLine(">>>   Pump Number : " + parameters[i].PumpNumber);
                Console.WriteLine(">>>   Pump State : " + parameters[i].PumpState);
                Console.WriteLine(">>>   Pump Power : " + parameters[i].ActPower);
            }
            PumpToXmlFormat(parameters);
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function CalculatePumpsParameters()");
        }
    }

    public static void PumpToXmlFormat(List<PumpParameters> lisToSend)
    {
        foreach (var a in lisToSend)
        {
            using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/Pump.xml", System.Text.Encoding.UTF8))
            {
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartElement("Pump");
                writer.WriteStartElement("Station");
                writer.WriteString(Convert.ToString(Station));
                writer.WriteEndElement();
                writer.WriteStartElement("PumpNumber");
                writer.WriteString(Convert.ToString(a.PumpNumber));
                writer.WriteEndElement();
                writer.WriteStartElement("PumpState");
                writer.WriteString(Convert.ToString(a.PumpState));
                writer.WriteEndElement();
                writer.WriteStartElement("SummaryPower");
                writer.WriteString(Convert.ToString(a.SumPower));
                writer.WriteEndElement();
                writer.WriteStartElement("ActivePower");
                writer.WriteString(Convert.ToString(a.ActPower));
                writer.WriteEndElement();
                writer.WriteStartElement("Amper1");
                writer.WriteString(Convert.ToString(a.Current_1));
                writer.WriteEndElement();
                writer.WriteStartElement("Amper2");
                writer.WriteString(Convert.ToString(a.Current_2));
                writer.WriteEndElement();
                writer.WriteStartElement("Amper3");
                writer.WriteString(Convert.ToString(a.Current_3));
                writer.WriteEndElement();
                writer.WriteStartElement("Power1");
                writer.WriteString(Convert.ToString(a.Power_1));
                writer.WriteEndElement();
                writer.WriteStartElement("Power2");
                writer.WriteString(Convert.ToString(a.Power_2));
                writer.WriteEndElement();
                writer.WriteStartElement("Power3");
                writer.WriteString(Convert.ToString(a.Power_3));
                writer.WriteEndElement();
                writer.WriteStartElement("Voltage1");
                writer.WriteString(Convert.ToString(a.Voltage_1));
                writer.WriteEndElement();
                writer.WriteStartElement("Voltage2");
                writer.WriteString(Convert.ToString(a.Voltage_2));
                writer.WriteEndElement();
                writer.WriteStartElement("Voltage3");
                writer.WriteString(Convert.ToString(a.Voltage_3));
                writer.WriteEndElement();
                writer.WriteStartElement("WorkingTime");
                writer.WriteString(Convert.ToString(a.WorkinTime));
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                String sendString = File.ReadAllText("/usr/local/bin/Pump.xml");
                File.Delete("/usr/local/bin/Pump.xml");
                Console.WriteLine(">>>   " + sendString);
                Send(sendString);
            }
        }
    }

    public static void Ergo(List<Ergo125> ergoList, int setFrequency)
    {
        int ergoCount = 0;

        while (true)
        {
            foreach (Ergo125 ergo in ergoList)
            {
                try
                {
                    Console.WriteLine("--------------------------------------");
                    Console.WriteLine(">>>   Address = " + ergo.serialNumber);
                    Console.WriteLine(">>>   Mode = " + ergo.mode);
                    Console.WriteLine(">>>   Baud = " + ergo.baudRate);
                    Console.WriteLine(">>>   RS Type = " + ergo.rsType);
                    Console.WriteLine("--------------------------------------");

                    String xml = String.Empty;

                    if (ergo.mode.ToLower().Contains("modbus"))
                    {
                        ergo.ReadValuessModbus(ergo.baudRate);
                    }
                    else
                    {
                        ergo.ReadValuesErgo(ergo.baudRate);
                    }

                    if (ergo.current.Any())
                    {
                        ergoCount = 0;
                        foreach (var item in ergo.current)
                        {
                            try
                            {
                                lock (sendLocker)
                                    RaspberryParameters.Add(new Parameters(item));
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                continue;
                            }
                        }
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        Thread.Sleep(10000);
                        throw new Exception("Not readed ergo");
                    }

                    lock (commandsLocker)
                    {
                        commandsList = commandsList.Distinct().ToList();

                        for (int i = 0; i < commandsList.Count; i++)
                        {
                            Console.WriteLine("Commands count in Ergo() : " + commandsList.Count);
                            try
                            {

                                if (commandsList[i].SerialNumber.Trim() == ergo.serialNumber.Trim())
                                {
                                    Console.WriteLine(">>>   Try to execute command : " + commandsList[i].SerialNumber + " - " + commandsList[i].DateReadFor);

                                    if (ergo.ReadArchivesValuesErgo(ergo.baudRate, commandsList[i].DateReadFor))
                                    {
                                        Console.WriteLine(">>>   Commands execute succesfully.");
                                        String toSend = "Archives-" + ergo.serialNumber + "-" + commandsList[i].CommandID + "-1\\";

                                        foreach (var itemList in ergo.archives)
                                        {
                                            foreach (var item in itemList)
                                            {
                                                toSend += item.SerialNumber + '$' + item.ParameterObisCode + '$' + item.ParameterNumber + '$' + item.ParameterValue + '$' + item.ParameterEquipmentTypeID + "$" + item.DateOfRead + '#';
                                            }
                                            toSend += '\n';
                                        }


                                        try
                                        {
                                            TcpClient client = new TcpClient(Server, Convert.ToInt32(Port));
                                            client.Client.Send(Encoding.ASCII.GetBytes("Archives"));
                                            Thread.Sleep(1000);

                                            client.Client.Send(Encoding.ASCII.GetBytes(toSend));
                                            commandsList.Remove(commandsList[i]);

                                            client.Client.Disconnect(false);
                                            client.Client.Close();

                                            client.Close();
                                        }
                                        catch (Exception e)
                                        {
                                            Thread.Sleep(TimeSpan.FromSeconds(10));
                                            Console.WriteLine(">>>   " + e.Message + "sendArchviesToServer()");
                                        }
                                    }
                                }
                                else
                                    Console.WriteLine(">>>   Commands execute failed.");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    Console.WriteLine(">>>   Ergo error count = " + ergoCount);
                    Console.WriteLine(">>>   " + ex.Message + " Ergo()");
                    ErrorWord = 5;

                    if (++ergoCount >= 20)
                        System.Environment.Exit(-1);
                    Console.WriteLine(">>>   " + ex.Message);
                }
            }
            Thread.Sleep(TimeSpan.FromSeconds(setFrequency));
        }
    }

    /// <summary>
    /// Function then are running in thread. Using for read data from NIK with several interval.
    /// </summary>
    /// <param name="name">Device name</param>
    /// <param name="addr">NIK unique address. It's serial number.</param>
    /// <param name="setFrequency">Frequency of reading in seconds</param>
    public static void ReadNik(String name, String addr, int setFrequency)
    {
        Console.WriteLine(">>>   " + name + "- Adress : " + addr);

        NIK nik = new NIK(Convert.ToInt32(addr), RaspberryID);

        while (true)
        {
            try
            {
                nik.ReadCurrent();

                foreach (var item in nik.values)
                    lock (sendLocker)
                        RaspberryParameters.Add(item);

                if (PumpsPower.Count > 0)
                {
                    CalculatePumpsParameters(nik.nik);
                }
                Thread.Sleep(TimeSpan.FromSeconds(setFrequency));
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " NIK()");
                ErrorWord = 5;
                setFrequency += 1;
                Thread.Sleep(5000);

                if (++count >= 10)
                    Environment.Exit(-1);
            }
        }
    }

    /// <summary>
    /// Function for reading data from Irka waterFlow device.
    /// </summary>
    /// <param name="address">Address of device need to be readed</param>
    private static void ReadIrka(int address)
    {
        byte[] receive = new byte[64];
        try
        {
            port = new SerialPort("/dev/serial0", 19200, Parity.None, 8, StopBits.One);
            port.Handshake = Handshake.None;
            port.ReadTimeout = 2000;
            port.WriteTimeout = 2000;
            port.Open();

            byte[] Moment_Quantity_Packet = { 0x2a, Convert.ToByte(address), 0x41, 0x6b };
            byte[] Counter_Quantity_Packet = { 0x2a, Convert.ToByte(address), 0x43, 0x6b };

            byte control_sum = 0;
            for (int i = 0; i < Moment_Quantity_Packet.Length - 1; i++) control_sum += Moment_Quantity_Packet[i];
            control_sum %= 0xff;
            Moment_Quantity_Packet[Moment_Quantity_Packet.Length - 1] = control_sum;

            control_sum = 0;
            for (int i = 0; i < Counter_Quantity_Packet.Length - 1; i++) control_sum += Counter_Quantity_Packet[i];
            control_sum %= 0xff;

            Counter_Quantity_Packet[Counter_Quantity_Packet.Length - 1] = control_sum;
            port.Write(Moment_Quantity_Packet, 0, Moment_Quantity_Packet.Length);
            Thread.Sleep(1000);
            receive = ReadPacket(port);

            String momentum = String.Empty, Counter = String.Empty;

            for (int i = 3; i < receive.Length - 3; i++) momentum += (char)receive[i];

            port.Write(Counter_Quantity_Packet, 0, Counter_Quantity_Packet.Length);
            Thread.Sleep(1000);
            receive = ReadPacket(port);

            for (int i = 3; i < receive.Length - 3; i++) Counter += (char)receive[i];

            string test = String.Empty, test1 = String.Empty;
            foreach (char c in momentum)
            {
                if (((int)c >= 48 && (int)c <= 58) || c == '.' || c == ',') test += c;
            }
            foreach (char c in Counter)
            {
                if (((int)c >= 48 && (int)c <= 58) || c == '.' || c == ',') test1 += c;
            }
            Console.WriteLine(">>>   " + test);
            Console.WriteLine(">>>   " + test1);
            test = test.Replace(".", ",");
            test1 = test1.Replace(".", ",");
            port.Close();

            String xml = WriteToXmlIrka(Convert.ToString(address), test, test1);
            Console.WriteLine(xml);
            Send(xml);
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + "Read Irka");
            Thread.Sleep(10000);
        }
    }

    private static string WriteToXmlIrka(String address, String moment, String count)
    {
        string xml = String.Empty;
        try
        {
            using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/Irka.xml", System.Text.Encoding.UTF8))
            {
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartElement("Irka");
                writer.WriteStartElement("Address");
                writer.WriteString(address);
                writer.WriteEndElement();
                writer.WriteStartElement("Momentum");
                writer.WriteString(Convert.ToString(moment));
                writer.WriteEndElement();
                writer.WriteStartElement("Counter");
                writer.WriteString(Convert.ToString(Convert.ToString(count)));
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
            xml = File.ReadAllText("/usr/local/bin/Irka.xml");
        }
        catch (Exception ex) { Console.WriteLine(">>>   " + ex.Message); }
        return xml;
    }

    /// <summary>
    /// Send string to Server.exe
    /// </summary>
    /// <param name="xml">String for sending</param>
    public static void Send(string StringData)
    {
        TcpClient client;

        //StringData = Encrypt(StringData, key);
        byte[] data = Encoding.ASCII.GetBytes(StringData);

        try
        {
            client = new TcpClient(Server, Convert.ToInt32(Port)); // create TCP client with server and port, when we create client by this construct it connects to server

            client.SendTimeout = 2000;
            client.ReceiveTimeout = 2000;
            Console.WriteLine(">>>   " + Server + " - " + Port + " - " + StringData);

            client.Client.Send(data); // send data to server

            client.Client.Disconnect(false); // disconnect from server and close client
            client.Client.Close();
            count = 0;
        }
        catch (Exception ex)
        {
            Thread.Sleep(2000);
            Console.WriteLine(">>>   " + ex.Message + " function Send()");

            lock (errorLocker)
            {
                ErrorWord = 4; // Error with network
            }
            if (++count >= 20)
            {
                System.Environment.Exit(-1);
            }
        }
    }

    /// <summary>
    /// Read values from ADC sensors and add it to global list
    /// </summary>
    /// <param name="name">Parameter name</param>
    /// <param name="SampleFrequency">Frequency for reading interval</param>
    public static void ADC(String name, int SampleFrequency)
    {
        Console.WriteLine(">>>   " + name + " Station - " + Station);
        double value = 0;
        int obisCode = 1;

        while (true) // read from adc in while loop
        {
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    var info = new ProcessStartInfo("sudo", "/usr/local/bin/i2c " + i); // run procces to read from adc where i - adc channel

                    info.UseShellExecute = false;
                    info.CreateNoWindow = true;
                    info.RedirectStandardOutput = true;
                    info.RedirectStandardError = true;

                    var p = Process.Start(info);

                    while (!p.StandardOutput.EndOfStream) // whle procees is running
                    {
                        string err = p.StandardOutput.ReadLine();
                        if (err.Contains("Result"))
                        {
                            err = err.Replace("Result", "");
                            value = Convert.ToDouble(err);
                            Console.WriteLine(">>>   ADC Value = " + value);
                        }
                    }
                    p.WaitForExit(); // wait process exit

                    Console.WriteLine(">>>   ADC process exit code = " + p.ExitCode);

                    if (p.ExitCode != 0)
                    {
                        Console.WriteLine(">>>   Problems with i2c Driver!");
                        Thread.Sleep(TimeSpan.FromSeconds(5));
                    }
                    else
                    {
                        Console.WriteLine(">>>   Pressure from ADC sensor " + i + " = " + value);

                        lock (sendLocker)
                            RaspberryParameters.Add(new Parameters(RaspberryID, obisCode, i, Convert.ToDouble(value), Convert.ToInt32(EquipmentTypeID))); // add data to List of valuess
                    }
                }
                Thread.Sleep(TimeSpan.FromSeconds(SampleFrequency)); // sleep to next reading
            }
            catch (Exception ex)
            {
                if (count++ >= 5)
                    Environment.Exit(-1);

                Console.WriteLine(">>>   " + ex.Message + " function ADC()");
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }
    }

    public static Int32 GetCounter()
    {
        Console.WriteLine(">>>   " + "Start getCounter()");
        int counter = 0, lastState = 0, currentState = 0;
        int watchDogTimer = 0, BitIndex = 0, BitData = 0;

        GPIO gpioCLK = Export("24", "in");
        GPIO gpioData = Export("10", "in");

        lastState = gpioCLK.GPIOGetValue();

        while (true)
        {
            currentState = gpioCLK.GPIOGetValue();

            if (lastState != currentState)
            {
                watchDogTimer = 0;
                if (currentState == 1)
                {
                    Thread.Sleep(10);

                    BitData = gpioData.GPIOGetValue();

                    if (BitData == 1)
                    {
                        counter |= (1 << BitIndex);
                    }

                    if (BitIndex < 31) BitIndex = BitIndex + 1;
                    else return counter;
                }
                lastState = currentState;
            }
            else
            {
                watchDogTimer = watchDogTimer + 1;

                if (watchDogTimer >= 50)
                {
                    watchDogTimer = 0;
                    BitIndex = 0;
                    counter = 0;
                }
                Thread.Sleep(10);
            }
        }
    }

    public static void ReadCounter(String name, String pin, String ID, String Station, long counter, int SampleFrequency)
    {
        try
        {
            Console.WriteLine(">>>   " + name + " pin - : " + pin + " ID - : " + ID + " station - : " + Station);

            Thread sendImpulses = new Thread(unused => sendValue(name, ref counter, SampleFrequency));
            sendImpulses.IsBackground = true;
            sendImpulses.Start();

            while (true)
            {
                counter = GetCounter();
                Thread.Sleep(30);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function ReadCounter()");
            Thread.Sleep(TimeSpan.FromSeconds(10));
        }
    }

    public static void Optron(String name, String pin, string ID, string Station, int SampleFrequency)
    {
        Console.WriteLine(">>>   " + name + " pin - : " + pin + " ID - : " + ID + " station - : " + Station);

        long value = 0;

        Thread sendValueThread = new Thread(unused => sendValue(name, ref value, SampleFrequency));
        sendValueThread.IsBackground = true;
        sendValueThread.Start();

        while (true)
        {
            try
            {
                value = Convert.ToInt32(Optron(pin));
                Thread.Sleep(1);
                count = 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " function Optron()");

                lock (errorLocker)
                {
                    if (name.Contains("moveSensor")) ErrorWord = 2;
                    if (name.Contains("Starter")) ErrorWord = 3;
                }

                if (++count >= 10)
                    Environment.Exit(-1);
            }
        }
    }

    /// <summary>
    /// Add value to List of values
    /// </summary>
    /// <param name="name">Value name</param>
    /// <param name="value">Value</param>
    /// <param name="SampleFrequency">Frequency for reading interval</param>
    public static void sendValue(string name, ref long value, int SampleFrequency)
    {
        while (true)
        {
            try
            {
                switch (name)
                {
                    case "pulseCounter":
                        lock (sendLocker)
                            RaspberryParameters.Add(new Parameters(RaspberryID, 65, 0, Convert.ToDouble(value), 15));
                        break;
                    case "moveSensor":
                        lock (sendLocker)
                            RaspberryParameters.Add(new Parameters(RaspberryID, 68, 0, Convert.ToDouble(value), 15));
                        break;
                    case "Starter":
                        lock (sendLocker)
                            RaspberryParameters.Add(new Parameters(RaspberryID, 78, 0, Convert.ToDouble(value), 15));
                        break;
                    case "ErrorWord":
                        lock (sendLocker)
                            RaspberryParameters.Add(new Parameters(RaspberryID, 48, 0, Convert.ToDouble(value), 15));
                        break;
                }
                Thread.Sleep(TimeSpan.FromSeconds(SampleFrequency));
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " function SendValue()");
                Thread.Sleep(TimeSpan.FromSeconds(10));
            }
        }
    }

    /// <summary>
    /// Set rele start state
    /// </summary>
    public static void outStart(GPIO gpio, bool val)
    {
        try
        {
            int state = (val == true ? 1 : 0);
            gpio.GPIOSetValue(Convert.ToString(state));
        }
        catch (Exception ex) { Console.WriteLine(">>>   " + ex.Message + " function outStart()"); }
    }

    /// <summary>
    /// Set rele stop state
    /// </summary>
    public static void outStop(GPIO gpio, bool val)
    {
        try
        {
            int state = (val == true ? 1 : 0);
            gpio.GPIOSetValue(Convert.ToString(state));
        }
        catch (Exception ex) { Console.WriteLine(">>>   " + ex.Message + " function outStop()"); }
    }

    /// <summary>
    /// Get rele start state
    /// </summary>
    /// <returns>return it's state</returns>
    public static bool inStart()
    {
        return (ReleOneState == 1 ? true : false);
    }

    /// <summary>
    /// Get rele stop state
    /// </summary>
    /// <returns>return it's state</returns>
    public static bool inStop()
    {
        return (ReleTwoState == 1 ? true : false);
    }

    public static bool Optron(String pin)
    {
        GPIO gpio = new GPIO(pin);

        int timer = 20, lastState = 1, currentState = 0, state = 0;

        currentState = gpio.GPIOGetValue();
        lastState = currentState;

        try
        {
            while (timer > 0)
            {
                currentState = gpio.GPIOGetValue();
                if (lastState != currentState) state = 1;
                timer--;
                Thread.Sleep(10);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function Optron()");
        }
        return (state == 1 ? true : false);
    }

    /// <summary>
    /// Call process to up resistors <para/>
    /// Run /usr/local/bin/Up program
    /// </summary>
    public static void PullUpResistors()
    {
        try
        {
            var info = new ProcessStartInfo();
            info.FileName = "sudo";
            info.Arguments = "/usr/local/bin/Up";

            info.UseShellExecute = false;
            info.CreateNoWindow = true;

            info.RedirectStandardOutput = true;
            info.RedirectStandardError = true;

            var p = Process.Start(info);
            p.WaitForExit();
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + "function PullUpResistors()");
        }
    }

    /// <summary>
    /// Function thar Create GPIO Object and Export it
    /// </summary>
    /// <param name="pin">GPIO pin number</param>
    /// <param name="direction">GPIO pin direction</param>
    /// <returns>Created GPIO Object</returns>
    public static GPIO Export(string pin, string direction)
    {
        GPIO gpio = new GPIO(pin);

        try
        {
            gpio.GPIOExport();
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " function Export()");
            try
            {
                Thread.Sleep(100);
                gpio.GPIOUnexport();
                Thread.Sleep(100);
                gpio.GPIOExport();
            }
            catch (Exception e)
            {
                Console.WriteLine(">>>   " + e.Message + " function Export()");
                Console.WriteLine(">>>   " + "GPIO Export failed");
            }
        }
        gpio.GPIOSetDirection(direction);
        return gpio;
    }


    /// <summary>
    /// Checks socket connection
    /// </summary>
    /// <param name="socket">Socket that you need to check</param>
    /// <returns>True or false value that indicates socket connection</returns>
    public static bool IsConnected(Socket socket)
    {
        try
        {
            return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0); // detect for socket connection
        }
        catch (SocketException) { return false; }
    }

    /// <summary>
    /// Function that connect to server and in while(true) loop get command from it
    /// </summary>
    public static void GetCommands()
    {
        while (true)
        {
            Stopwatch stopWatch = new Stopwatch();

            try
            {

                TcpClient client = new TcpClient(Server, Convert.ToInt32(Port));

                client.Client.ReceiveTimeout = 5000;
                client.Client.SendTimeout = 5000;

                NetworkStream stream = new NetworkStream(client.Client);

                stream.Write(Encoding.ASCII.GetBytes("Commands"), 0, "Commands".Length);

                stopWatch.Start();

                while (true)
                {
                    try
                    {
                        Console.WriteLine(">>>   Commands Count : " + commandsList.Count);

                        if (!(IsConnected(client.Client)))
                            throw new Exception("Socket is not Connected");

                        if (stream.DataAvailable)
                        {
                            byte[] receive = new byte[65535];

                            int commanSize = stream.Read(receive, 0, receive.Length);
                            receive = receive.Take(commanSize).ToArray();

                            String data = Encoding.ASCII.GetString(receive);
                            String[] commands = data.Split('\n');

                            foreach (String cmd in commands)
                            {
                                String[] cmdInfo = cmd.Split('$');

                                lock (commandsLocker)
                                {
                                    try
                                    {
                                        Console.Write(">>>   Reecived command : " + cmdInfo[0] + " - " + cmdInfo[1] + " - " + cmdInfo[2]);
                                        commandsList.Add(new Command(cmdInfo[1], Convert.ToInt32(cmdInfo[0]), Convert.ToDateTime(cmdInfo[2])));
                                    }
                                    catch (Exception ee)
                                    {
                                        Console.WriteLine(">>>   " + ee.Message + "ReceiveCommands()");
                                    }
                                }
                            }
                        }
                        Thread.Sleep(2000);

                        TimeSpan ts = stopWatch.Elapsed;
                        if (ts.Minutes >= 5)
                            throw new Exception("Reconnect to server");
                    }
                    catch (Exception e)
                    {
                        stopWatch.Stop();
                        stopWatch.Reset();

                        Thread.Sleep(2000);
                        Console.WriteLine(">>>   " + e.Message);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(2000);
            }
        }
    }

    public static void ReleWork(String ParameterID_ReleOne, String ParameterID_ReleTwo)
    {
        TcpClient client;
        NetworkStream stream;

        bool b_Stop = false;
        bool b_Start = false;
        bool Everything_OK = true; // переменная все_ок
        byte Timer_Ticks = 2; // 1c
        byte Last_State = 0;
        bool Interlock_Status = true; // True - OK 
        byte btState = 0;
        bool Raspberry_Ready; // готовность малинки
        bool bIN_Start = false;
        bool bIN_Stop = false;
        bool Raspberry_Status = false;
        bool bEN;
        bool bIN_Feedback;
        int iTimer_Start = 0;
        int iTimer_Stop = 0;
        bool First_Init = true;

        Console.WriteLine(">>>   " + "Last current state = " + LastCurrentState);
        if (LastCurrentState == 1) Last_State = 2;

        while (true)
        {
            try
            {
                client = new TcpClient(Server, Convert.ToInt32(Port));
                stream = new NetworkStream(client.Client);

                stream.ReadTimeout = 5000;
                stream.WriteTimeout = 5000;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " function ReleWork() - connect to server part");
                errorCounter = errorCounter + 1;

                if (errorCounter >= 20)
                    System.Environment.Exit(-1);
                Thread.Sleep(5000);
                continue;
            }

            while (true) // waiting for changes
            {
                try
                {
                    errorCounter = 0;
                    if (First_Init == true)
                    {
                        First_Init = false;
                        btState = 0;
                    }

                    if (!(IsConnected(client.Client))) // Code for not sending request to server, but server send result if value in DB was changed
                    {
                        try
                        {
                            stream.Close();
                            client.Client.Disconnect(false);
                            client.Client.Close();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(">>>   " + ex.Message + " function ReleWork() problems with socket");
                        }
                        throw new Exception(">>>   " + "Socket is not connected!");
                    }

                    if (stream.DataAvailable)
                    {
                        Console.WriteLine(">>>   " + "Stream data available");

                        byte[] receive = new byte[2];
                        int bytesRead = stream.Read(receive, 0, receive.Length);
                        receive = receive.Take(bytesRead).ToArray();

                        ReleOneState = Convert.ToInt32(receive[0]);
                        ReleTwoState = Convert.ToInt32(receive[1]);
                        Console.WriteLine(">>>   " + "Rele1 - " + receive[0]);
                        Console.WriteLine(">>>   " + "Rele2 - " + receive[1]);
                    }

                    bIN_Start = Convert.ToBoolean(ReleOneState);
                    bIN_Stop = Convert.ToBoolean(ReleTwoState);
                    bIN_Feedback = Optron(starterPin);
                    Thread.Sleep(1000);

                    Console.WriteLine(">>>   " + "Rele-1 state : " + bIN_Start);
                    Console.WriteLine(">>>   " + "Rele-2 state : " + bIN_Stop);
                    Console.WriteLine(">>>   " + "Feedback state : " + bIN_Feedback);

                    if (bIN_Start == true) // отслеживаем верхний фронт сигнала пуска
                    {
                        b_Start = true;
                        // Reset(ParameterID_ReleOne);
                    }
                    else bIN_Start = false;
                    if (bIN_Stop == true) // отслеживаем верхний фронт сигнала стопа
                    {
                        b_Stop = true;
                        //Reset(ParameterID_ReleTwo);
                    }
                    else bIN_Stop = false;

                    switch (btState)
                    {
                        case 0:  // Состояние Инициализация
                            {
                                Console.WriteLine(">>>   " + "State 0");
                                Raspberry_Status = false;
                                bEN = false;
                                bIN_Feedback = false;
                                b_Start = false;
                                b_Stop = false;
                                outStart(gpioReleOne, false);
                                outStop(gpioReleTwo, false);

                                iTimer_Start = Timer_Ticks; // 10 тиков соответствует 1 секунде
                                iTimer_Stop = Timer_Ticks; // 10 тиков соответствует 1 секунде
                                btState = 1; // перейти в состояние нормальный стоп
                                if (LastCurrentState == 1) btState = 2;
                                Last_State = 0;
                                // Состояние Инициализация Конец
                                break;
                            }
                        case 1:  // Состояние нормальный стоп
                            {
                                Console.WriteLine(">>>   " + "State 1");
                                b_Stop = false;
                                Raspberry_Status = false; // узел выключен
                                Raspberry_Ready = true;


                                bEN = true; // разрешить запуск 

                                Last_State = 1;


                                if ((b_Start == true) && // нажата кнопка старт в скаде

                                 (bEN == true)) // все в порядке
                                {
                                    btState = 2; // перейти в состояние попытка запуска
                                }


                                if (bIN_Feedback == true) // если включился контактор
                                {
                                    btState = 4; // перейти в состояние нормальная работа
                                    Last_State = 1;
                                }
                                break;
                                // Состояние нормальный стоп Конец
                            }

                        case 2:  // Состояние попытка запуска
                            {
                                Console.WriteLine(">>>   " + "State 2");

                                outStart(gpioReleOne, true); // включить контактор 
                                if (iTimer_Start == 0) // если таймер истек
                                {

                                    btState = 3; // перейти в состояние проверка запуска
                                    Last_State = 2;
                                }
                                else iTimer_Start = iTimer_Start - 1; // уменьшаем счетчик

                                break;
                                // Состояние попытка запуска Конец
                            }

                        case 3: // Состояние проверка запуска
                            {
                                Console.WriteLine(">>>   " + "State 3");
                                iTimer_Start = Timer_Ticks; // восстановим таймер
                                // Функция Everything_OK
                                Everything_OK = true;

                                if (bIN_Feedback == false)  // если не замкнулся пускатель
                                    Everything_OK = false;

                                if (Everything_OK == true) // или же это форсированый старт
                                    bEN = true; // разрешить запуск 
                                else
                                    bEN = false; // запуск не разрешен
                                // Функция Everything_OK конец
                                outStart(gpioReleOne, false);// сбросить команду на включение, выход импульсный, тоесть контактор должен был автоблокироваться.

                                if (Everything_OK == true)
                                {
                                    btState = 4; // перейти в состояние нормальная работа
                                    Last_State = 3;
                                }
                                else
                                {
                                    btState = 5;
                                    outStop(gpioReleTwo, true);
                                    Last_State = 3;
                                }
                                break;

                                // Состояние проверка запуска Конец
                            }

                        case 4:     // Состояние нормальная работа
                            {
                                Console.WriteLine(">>>   " + "State 4");
                                b_Start = false;
                                Raspberry_Status = true; // узел включен
                                // Функция Everything_OK
                                Everything_OK = true;

                                if (Everything_OK == true) // или же это форсированый старт
                                    bEN = true; // разрешить запуск 
                                else
                                    bEN = false; // запуск не разрешен
                                // Функция Everything_OK конец

                                if (bIN_Feedback == false)   // самопроизвольно выключился контактор
                                {
                                    btState = 1; // перейти в состояние нормальный стоп
                                    Last_State = 4;
                                }

                                if ((Everything_OK == true) && (b_Stop == true))
                                {
                                    // оператор выключил элемент, тут еще нужно добавить интерлок
                                    // еще нужно предусмотреть ошибку двойного нажатия клавиш, стоп и старт одновременно
                                    btState = 6; // перейти в состояние попытка стопа
                                    Last_State = 4;
                                }
                                if (Everything_OK == false)
                                {

                                    btState = 5; // перейти в состояние аварийный стоп
                                    outStop(gpioReleTwo, true);// выключить контактор
                                    Last_State = 4;
                                }
                                break;
                                // Состояние нормальная работа Конец
                            }
                        case 5: // Состояние аварийный стоп
                            {
                                Console.WriteLine(">>>   " + "State 5");
                                Raspberry_Ready = false;
                                // тут будет обрабатыватся авария.
                                // а также ожидание на переход в нормальный режим по команде оператора.
                                Raspberry_Status = false; // узел выключен
                                b_Stop = false;
                                b_Start = false;

                                // Функция Everything_OK
                                Everything_OK = true;

                                if (Everything_OK == true)
                                {
                                    // ошибка сброшена
                                    outStop(gpioReleTwo, false); // сбросить сигнал выключить контактор
                                    btState = 1; // перейти в состояние нормальный стоп
                                    Last_State = 5;
                                }
                                // Состояние аварийный стоп Конец
                                break;
                            }
                        case 6: // Состояние попытка стопа
                            {

                                Console.WriteLine(">>>   " + "State 6");
                                outStop(gpioReleTwo, true); // выключить контактор 
                                if (iTimer_Stop == 0)  // если таймер истек
                                {
                                    btState = 7; // перейти в состояние проверка остановки
                                    Last_State = 6;
                                }
                                else iTimer_Stop = iTimer_Stop - 1; // уменьшаем счетчик
                                break;
                                // Состояние попытка стопа Конец
                            }
                        case 7: // Состояние проверка стопа
                            {
                                Console.WriteLine(">>>   " + "State 7");
                                iTimer_Stop = Timer_Ticks; // восстановим таймер
                                // Функция Everything_OK
                                Everything_OK = true;

                                if (bIN_Feedback == true)  // если не разомкнулся пускатель
                                    Everything_OK = false;

                                if (Everything_OK == true) // или же это форсированый старт
                                    bEN = true; // разрешить запуск 
                                else bEN = false; // запуск не разрешен
                                // Функция Everything_OK конец
                                outStop(gpioReleTwo, false); // сбросить команду на выключение, выход импульсный.

                                if (Everything_OK == true)
                                {
                                    btState = 1; // перейти в состояние нормальный стоп
                                    Last_State = 7;
                                }
                                else
                                {
                                    btState = 5; // перейти в состояние аварийный стоп
                                    Last_State = 7;
                                }
                                break;
                                // Состояние проверка стопа Конец
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>>   " + ex.Message + " function ReleWork()");
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Create packet and send it to Server.exe <para/>
    /// Packet example = id $ obisCode $ parameterNumber $ value $ equipmnetTypeID #
    /// </summary>
    public static void SendData()
    {
        while (true)
        {
            try
            {
                Console.WriteLine(">>>   " + Server + " - " + Port);
                String toSend = String.Empty;

                lock (sendLocker)
                {
                    foreach (var item in RaspberryParameters)
                    {
                        Console.WriteLine(item.ParameterObisCode + " " + item.ParameterNumber + " " + item.ParameterValue);
                        toSend += item.SerialNumber + '$' + item.ParameterObisCode + '$' + item.ParameterNumber + '$' + item.ParameterValue + '$' + item.ParameterEquipmentTypeID + '#';
                    }
                    RaspberryParameters.Clear();
                }

                Console.WriteLine(">>>   Data String " + toSend);
                if (toSend != String.Empty)
                {
                    Send(toSend);
                }
                Thread.Sleep(TimeSpan.FromSeconds(30));
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendData =>>>> " + ex.Message);
                Thread.Sleep(TimeSpan.FromSeconds(10));
            }
        }
    }

    /// <summary>
    /// Create all neccessary treads running in Program
    /// </summary>
    public static void CreateTheadsList()
    {
        try
        {
            int i = 0;
            while (i < RaspberryInfo.Count)
            {
                switch (RaspberryInfo[i].ToLower())
                {
                    case "pulsecounter":
                        {
                            String name = RaspberryInfo[i];
                            String pin = RaspberryInfo[i + 1].ToLower().Replace("gpio=", "");
                            String ParamID = RaspberryInfo[i + 2].ToLower().Replace("parameterid=", "");
                            String SampleFrequency = RaspberryInfo[i + 3].ToLower().Replace("samplingfrequency=", "");
                            Export(pin, "in");

                            Thread pulseThread = new Thread(unused => ReadCounter(name, pin, ParamID, Station, counter, Convert.ToInt32(SampleFrequency)))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(pulseThread);
                            i = i + 5;
                        }
                        break;
                    case "movesensor":
                        {
                            String name = RaspberryInfo[i];
                            String pin = RaspberryInfo[i + 1].ToLower().Replace("gpio=", "");
                            String ParamID = RaspberryInfo[i + 2].ToLower().Replace("parameterid=", "");
                            String SampleFrequency = RaspberryInfo[i + 3].ToLower().Replace("samplingfrequency=", "");
                            Export(pin, "in");

                            Thread moveThread = new Thread(unused => Optron(name, pin, ParamID, Station, Convert.ToInt32(SampleFrequency)))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(moveThread);
                            i = i + 5;
                        }
                        break;
                    case "starter":
                        {
                            String name = RaspberryInfo[i];
                            String pin = RaspberryInfo[i + 1].ToLower().Replace("gpio=", "");
                            String ParamID = RaspberryInfo[i + 2].ToLower().Replace("parameterid=", "");
                            String SampleFrequency = RaspberryInfo[i + 3].ToLower().Replace("samplingfrequency=", "");
                            Export(pin, "in");

                            Thread starterThread = new Thread(unused => Optron(name, pin, ParamID, Station, Convert.ToInt32(SampleFrequency)))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(starterThread);
                            starterPin = pin;
                            i = i + 5;
                        }
                        break;
                    case "adc":
                        {
                            String name = RaspberryInfo[i];
                            String ParamID = RaspberryInfo[i + 1].ToLower().Replace("parameterid=", "");
                            String SampleFrequency = RaspberryInfo[i + 2].ToLower().Replace("samplingfrequency=", "");

                            Thread adcThread = new Thread(unused => ADC(name, Convert.ToInt32(SampleFrequency)))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(adcThread);
                            i = i + 4;
                        }
                        break;
                    case "nik":
                        {
                            String name = RaspberryInfo[i];
                            String Address = RaspberryInfo[i + 1].ToLower().Replace("address=", "");
                            String SampleFrequency = RaspberryInfo[i + 2].ToLower().Replace("samplingfrequency=", "");

                            Thread nikThread = new Thread(unused => ReadNik(name, Address, Convert.ToInt32(SampleFrequency)))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(nikThread);
                            i = i + 4;
                        }
                        break;
                    case "ergo":
                        {
                            String name = RaspberryInfo[i];
                            String Address = RaspberryInfo[i + 1].ToLower().Replace("address=", "");
                            String SampleFrequency = RaspberryInfo[i + 2].ToLower().Replace("samplingfrequency=", "");
                            String mode = RaspberryInfo[i + 3].ToLower().Replace("mode=", "");
                            String baud = RaspberryInfo[i + 4].ToLower().Replace("baud=", "");
                            String rsType = RaspberryInfo[i + 5].ToLower().Replace("rs=", "");
                            String inversion = RaspberryInfo[i + 6].ToLower().Replace("inversion=", "");

                            Ergo125 e = new Ergo125(Address, mode, baud, rsType, SampleFrequency, Convert.ToBoolean(inversion));

                            ergoList.Add(e);

                            i = i + 8;
                        }
                        break;
                    case "reles":
                        {
                            String pinRele1 = RaspberryInfo[i + 1].ToLower().Replace("releonepin=", "");
                            String pinRele2 = RaspberryInfo[i + 2].ToLower().Replace("reletwopin=", "");
                            String parID_ReleOne = RaspberryInfo[i + 3].ToLower().Replace("parameterid_releone=", "");
                            String parID_ReleTwo = RaspberryInfo[i + 4].ToLower().Replace("parameterid_reletwo=", "");

                            gpioReleOne = Export(pinRele1, "out");
                            gpioReleTwo = Export(pinRele2, "out");

                            Thread releThread = new Thread(unused => ReleWork(parID_ReleOne, parID_ReleTwo))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(releThread);
                            i = i + 6;
                        }
                        break;
                    case "pumpspowers":
                        {
                            i = i + 1;
                            while (RaspberryInfo[i] != "pumpspowers")
                            {
                                PumpsPower.Add(Convert.ToInt32(RaspberryInfo[i].ToLower().Replace("power=", "")));
                                parameters.Add(new PumpParameters()); // add pump to global structure
                                parameters.Last().lastReading = DateTime.Now; // init time of last reading for workTime
                                GPIOPumps.Add(Export(RaspberryInfo[i + 1].ToLower().Replace("gpio=", ""), "in")); // create gpio interface for getting pump state
                                i = i + 2;
                            }
                            i = i + 1;
                        }
                        break;
                    case "dozor-s":
                        {
                            String name = RaspberryInfo[i];
                            String SerialNumber = RaspberryInfo[i + 1].ToLower().Replace("serialnumber=", "");
                            String ID = RaspberryInfo[i + 2].ToLower().Replace("id=", "");
                            String EquipmentTypeID = RaspberryInfo[i + 3].ToLower().Replace("equipmenttypeid=", "");
                            String ComName = RaspberryInfo[i + 4].Replace("Com=", "");
                            ComName = ComName.Replace("com=", "");
                            String baud = RaspberryInfo[i + 5].ToLower().Replace("baud=", "");
                            String rsType = RaspberryInfo[i + 6].ToLower().Replace("rs=", "");

                            Thread dozorThread = new Thread(unused => Dozor(SerialNumber, Convert.ToInt32(ID), Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(baud), rsType))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(dozorThread);

                            //GlobalDozor = new Dozor(SerialNumber, Convert.ToInt32(ID), Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(baud), rsType);

                            i += 8;
                        }
                        break;
                    case "dozimeter":
                        {
                            String name = RaspberryInfo[i];
                            String SerialNumber = RaspberryInfo[i + 1].ToLower().Replace("serialnumber=", "");
                            String ID = RaspberryInfo[i + 2].ToLower().Replace("id=", "");
                            String EquipmentTypeID = RaspberryInfo[i + 3].ToLower().Replace("equipmenttypeid=", "");
                            String ComName = RaspberryInfo[i + 4].Replace("Com=", "");
                            ComName = ComName.Replace("com=", "");
                            String baud = RaspberryInfo[i + 5].ToLower().Replace("baud=", "");
                            String rsType = RaspberryInfo[i + 6].ToLower().Replace("rs=", "");

                            Thread dozimeterThread = new Thread(unused => Dozimeter(SerialNumber, Convert.ToInt32(ID), Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(baud), rsType))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(dozimeterThread);

                            //GlobalDozor = new Dozor(SerialNumber, Convert.ToInt32(ID), Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(baud), rsType);

                            i += 8;
                        }
                        break;
                    case "meteostation":
                        {
                            String name = RaspberryInfo[i];
                            String ID = RaspberryInfo[i + 1].ToLower().Replace("id=", "");
                            String EquipmentTypeID = RaspberryInfo[i + 2].ToLower().Replace("equipmenttypeid=", "");
                            String ComName = RaspberryInfo[i + 3].Replace("Com=", "");
                            ComName = ComName.Replace("com=", "");
                            String Baud = RaspberryInfo[i + 4].ToLower().Replace("baud=", "");
                            String rsType = RaspberryInfo[i + 5].ToLower().Replace("rs=", "");

                            Thread meteoStationThread = new Thread(unused => MeteoStation(ID, Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(Baud), rsType))
                            {
                                IsBackground = true
                            };
                            ThreadList.Add(meteoStationThread);

                            //GlobalMeteoStation = new MeteoStation(ID, Convert.ToInt32(EquipmentTypeID), ComName, Convert.ToInt32(Baud), rsType);

                            i += 7;
                        }
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message + " CreateTrheadList()");
            Environment.Exit(-1);
        }
    }

    private static void MeteoStation(String ID, int EquipmentTypeID, String ComName, int baud, String rsType)
    {
        MeteoStation Station = new MeteoStation(ID, EquipmentTypeID, ComName, baud, rsType);

        while (true)
        {
            try
            {
                Station.ReadData();

                lock (sendLocker)
                {
                    RaspberryParameters.AddRange(Station.GetDataList());
                }
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(5000);
            }
        }
    }

    private static void Dozimeter(String SerialNumber, int ID, int EquipmentTypeID, String ComName, int BaudRate, String RsType)
    {
        Dozimeter dozimeter = new Dozimeter(SerialNumber, ID, EquipmentTypeID, ComName, BaudRate, RsType);

        while (true)
        {
            try
            {
                lock (serialLocker)
                {
                    dozimeter.ReadData();
                    Console.WriteLine("DozimeterData readed");
                }

                lock (sendLocker)
                {
                    Console.WriteLine("Dozimeter data added");
                    RaspberryParameters.AddRange(dozimeter.GetDataList());
                }
                Thread.Sleep(TimeSpan.FromSeconds(20));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(5000);
            }
        }
    }

    private static void Dozor(String SerialNumber, int ID, int EquipmentTypeID, String ComName, int BaudRate, String RsType)
    {
        Dozor dozor = new Dozor(SerialNumber, ID, EquipmentTypeID, ComName, BaudRate, RsType);

        while (true)
        {
            try
            {
                lock (serialLocker)
                {
                    dozor.ReadData();
                }

                lock (sendLocker)
                {
                    RaspberryParameters.AddRange(dozor.GetDataList());
                }
                Thread.Sleep(TimeSpan.FromSeconds(20));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(5000);
            }
        }
    }

    private static void EcologyWork()
    {
        while (true)
        {
            try
            {
                GlobalMeteoStation.ReadData();
                GlobalDozor.ReadData();

                lock (sendLocker)
                {
                    RaspberryParameters.AddRange(GlobalMeteoStation.GetDataList());
                    RaspberryParameters.AddRange(GlobalDozor.GetDataList());
                }
                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    /// <summary>
    /// Main function for starting Program
    /// </summary>
    public static void Main()
    {
        try // try to read Configuration.xml
        {
            RaspberryInfo = ReadFileLineByLine("/usr/local/bin/Configuration.xml");
            RaspberryInfo = ReplaceUnusedCharacters(RaspberryInfo);
            Console.WriteLine(">>>   Server - " + Server);
            Console.WriteLine(">>>   Port - " + Port);
            Console.WriteLine(">>>   StationID - " + Station);
            Console.WriteLine(">>>   RaspberryID - " + RaspberryID);
            Console.WriteLine(">>>   EquipmentTypeID - " + EquipmentTypeID);
            Console.WriteLine(">>>   Config is reading succesfull");
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>   " + ex.Message + " Read Configuration Error");
            Environment.Exit(-1);
        }

        try // Try to add subsidiaries threads to ThreadList
        {
            CreateTheadsList(); // main Convertion of Configuration.xml

            Thread SendErrorThread = new Thread(unused => sendValue("ErrorWord", ref ErrorWord, 30))
            {
                IsBackground = true
            };

            Thread SendDataThread = new Thread(SendData)
            {
                IsBackground = true
            };

            Thread UpResistorsThread = new Thread(PullUpResistors)
            {
                IsBackground = true
            };

            ThreadList.Add(UpResistorsThread); // Thread for Up resistors
            ThreadList.Add(SendDataThread); // Thread for sending data to Server.exe
            ThreadList.Add(SendErrorThread); // Thread for sendig error word

            /*Thread commands = new Thread(GetCommands); // Thread for receiving command from Server.exe
            commands.IsBackground = true;
            ThreadList.Add(commands);*/

            if (ergoList.Any())
            {
                Thread ergoThread = new Thread(unused => Ergo(ergoList, 30))
                {
                    IsBackground = true
                };
                ergoThread.Start(); // Thread that start working with Ergomeer
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(">>>    " + ex.Message + " Runnning subsidiaries Threads Error");
        }

        foreach (var thread in ThreadList) // run all working threads
        {
            thread.Start(); // start all threads
            Thread.Sleep(100);
        }

        if (GlobalDozor != null && GlobalMeteoStation != null)
        {
            Thread BaseEcologyThread = new Thread(x => EcologyWork())
            {
                IsBackground = true
            };
            BaseEcologyThread.Start();
        }

        while (true)
        {
            Thread.Sleep(200);
        }
    }
}
