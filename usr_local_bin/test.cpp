#include <stdint.h>
#include <bcm2835.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <unistd.h>
#include <string.h>
using namespace std;

#define OP1 9
#define OP2 11
#define PC 4
#define CLK 24
#define DATA 10
#define DIRECTION1 17
#define DIRECTION2 27

#define IN  0
#define OUT 1

int main()
{
    if (!bcm2835_init()) 
		return 1;

    bcm2835_gpio_fsel(OP1, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(OP1, 2);
    bcm2835_gpio_fsel(OP2, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(OP2, 2);
    bcm2835_gpio_fsel(PC, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(PC, 2);
    bcm2835_gpio_fsel(CLK, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(CLK, 2);
    bcm2835_gpio_fsel(DATA, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(DATA, 2);
    bcm2835_gpio_fsel(DIRECTION1, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(DIRECTION1, 2);
    bcm2835_gpio_fsel(DIRECTION2, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(DIRECTION2, 2);

    cout << "up" << endl;
}
