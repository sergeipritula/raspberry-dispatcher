#include <errno.h>
#include <linux/i2c-dev.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/fs.h>
#include <errno.h>
#include <stdint.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <termio.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
    printf("I2C Program started");
    int channel = 0;
    int fd = open("/dev/i2c-1", O_RDWR);

    if (argc == 2)
    {
		sscanf(argv[1], "%d", &channel);
    }

    printf("Channel : %d\n", channel);

    if (fd < 0)
    {
        printf("\n NOT ABLE TO OPEN THE DRIVER \n");
		return -1;
	}
	else
	{
	    printf("--I2C IS WORKING--\n");
	}

	char buf[100];         
	char buff[100];
	float valuee = 0, m1 = 0;  
	int i = 0;
	int  io, wbyte,rbyte;

	buf[0] = 0x48;
	buf[1] = channel;
	buf[2] = 0x91;
	io = ioctl(fd, 0x703, 0x48);

	if(io < 0)
	{
		printf("error ioctl\n");
		return -1;
	}
	else
	{
		wbyte = write(fd, buf, 3); 
		usleep(1 * 1000);
		if(wbyte != 3)
		{
			printf("error write:%d\n", wbyte);
			return -1;
		}

		rbyte=read(fd, buff, 10);
		printf("buff len %d\n", buff[rbyte]);
		sleep(2);
		
		for(i = 0; i <= 9; i++)  
		{                                           
			valuee=(buff[i] * 5.0) / 255;
			m1= m1 + valuee;
			printf("buff value %d\n", buff[i]);
			printf("ADC VALUE: %3.2fv\n", valuee);
		}
		
		m1 = m1 / 10.0f;
		m1 *= 5.0f;
		close(fd);
		printf("Result%f\n", m1);
		return 0;
	}
}
