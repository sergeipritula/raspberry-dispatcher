﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using System.Linq;
using System.Security.Cryptography;
using System.Diagnostics;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Net.Cache;
using MySql.Data.MySqlClient;

namespace Program
{
    public class GPIO
    {
        public string GPIOPin;

        public GPIO(string gnum)
        {
            GPIOPin = gnum;
        }

        public int GPIOExport()
        {
            string export_str = "/sys/class/gpio/export";
            if (File.Exists(export_str)) File.WriteAllText(export_str, GPIOPin);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOUnexport()
        {
            string unexport_str = "/sys/class/gpio/unexport";
            if (File.Exists(unexport_str)) File.WriteAllText(unexport_str, GPIOPin);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOSetDirection(string dir)
        {
            string setdir_str = "/sys/class/gpio/gpio" + GPIOPin + "/direction";
            if (File.Exists(setdir_str)) File.WriteAllText(setdir_str, dir);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOSetValue(string val)
        {
            string setval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
            if (File.Exists(setval_str)) File.WriteAllText(setval_str, val);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOGetValue()
        {
            string val = String.Empty;
            string getval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
            if (File.Exists(getval_str)) val = File.ReadAllText(getval_str);
            else throw new FileNotFoundException();
            return Convert.ToInt32(val);
        }
    }

    public class Program
    {
        private static string key = "IIltpohall", Station = String.Empty, Server = String.Empty, Port = String.Empty, RaspberryID = String.Empty;
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");
        private static long ErrorWord = 0, counter = 0, LastCurrentState = 0, totalTime = 0;
        private static int ReleOneState = 0, ReleTwoState = 0, count = 0, errorCounter = 0;
        private static List<PumpParameters> parameters = new List<PumpParameters>();
        private static List<DateTime> PumpDateTime = new List<DateTime>();
        private static List<Thread> ThreadList = new List<Thread>();
        private static List<String> devices = new List<string>();
        private static List<GPIO> GPIOPumps = new List<GPIO>();
        private static List<int> PumpsPower = new List<int>();
        private static string starterPin = String.Empty;
        private static object errorLocker = new object();
        private static object sendLocker = new object();
        private static object dbLocker = new object();
        private static object logsLocker = new object();
        private const int keysize = 256;
        private static SerialPort port;
        private static DateTime previous;

        /*  Error codes
            0 - No Problems
            1 - Problems with pulse counter
            2 - Problems with move sensor
            3 - Problems with starter
            4 - Problems with sending info to server
            5 - Problems with nik
            6 - Problems with power supply
            7 - Starter not start
            8 - Start rele not start
            9 - Stop rele not start
         */

        public static GPIO gpioReleOne;
        public static GPIO gpioReleTwo;

        static byte[] SNRM = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x93, 0x06, 0x13, 0x7e }; //frame SNRM
        static byte[] AARQ = { 0x7e, 0xa0, 0x50, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x0d, 0x3e, 0xe6, 0xe6, 0x00,
                               0x60, 0x3f ,0xa1, 0x09, 0x06, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x01, 0x01, 0x8a,
                               0x02, 0x07, 0x80 ,0x8b ,0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x02, 0x01, 0xac, 0x12,
                               0x80, 0x11, 0x00, 0x31, 0x31, 0x31 ,0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31,
                               0x31, 0x31, 0x31, 0x31, 0x31, 0xbe, 0x10, 0x04 ,0x0e, 0x01, 0x00, 0x00, 0x00, 0x06,
                               0x5f, 0x1f, 0x04, 0x00, 0x00, 0x08, 0xcb, 0x00, 0x80, 0x9c ,0x46, 0x7e }; // AARQ request for authorization by User with passord

        static byte[] DISC = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x53, 0x0a, 0xd5, 0x7e }; // frame DISC  


        //Requests for Parameters

        static byte[] readVoltage_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                        0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                        0x00, 0x20, 0x07, 0x00, 0x00, 0x02, 0x00, 0x73, 0x88, 0x7e };

        static byte[] readVoltage_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x93,
                                        0x17, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                        0x00, 0x34, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6f, 0xda, 0x7e };

        static byte[] readVoltage_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                        0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                        0x00, 0x48, 0x07, 0x00, 0x00, 0x02, 0x00, 0x9a, 0x2e, 0x7e };

        static byte[] readAmper_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x1f, 0x07, 0x00, 0x00, 0x02, 0x00, 0x2a, 0x72, 0x7e, };

        static byte[] readAmper_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x33, 0x07, 0x00, 0x00, 0x02, 0x00, 0xbe, 0xc6, 0x7e };

        static byte[] readAmper_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x47, 0x07, 0x00, 0x00, 0x02, 0x00, 0x13, 0x13, 0x7e };

        static byte[] readPower_A = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x15, 0x07, 0x00, 0x00, 0x02, 0x00, 0x84, 0xd6, 0x7e };

        static byte[] readPower_B = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x29, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6d, 0x70, 0x7e };

        static byte[] readPower_C = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48,
                                      0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                      0x00, 0x3D, 0x07, 0x00, 0x00, 0x02, 0x00, 0x24, 0x5b, 0x7e };

        static byte[] AvrPow = { 0x7E, 0xA0, 0x1C, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91, 0x48, 0xE6,
                                 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01, 0x00, 0x01, 0x07, 0x00,
                                 0x00, 0x02, 0x00, 0x38, 0x09, 0x7E };

        static byte[] SumPow = { 0x7E, 0xA0, 0x1C, 0x06, 0xA8, 0xAC, 0x01, 0x21, 0x10, 0x91, 0x48, 0xE6,
                                 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01, 0x00, 0x01, 0x08, 0x00,
                                 0xFF, 0x02, 0x00, 0x37, 0xA5, 0x7E};

        public struct NIK
        {
            public string SumPower;
            public string ActPower;

            public string Voltage_1;
            public string Voltage_2;
            public string Voltage_3;

            public string Current_1;
            public string Current_2;
            public string Current_3;

            public string Power_1;
            public string Power_2;
            public string Power_3;

            public long WorkinTime;
        }

        public class PumpParameters
        {
            public int PumpNumber;
            public int PumpState;
            public double PumpNominalPower;
            public double coefficient;

            public double SumPower;
            public double ActPower;

            public string Voltage_1;
            public string Voltage_2;
            public string Voltage_3;

            public string Current_1;
            public string Current_2;
            public string Current_3;

            public string Power_1;
            public string Power_2;
            public string Power_3;
            public long WorkinTime;
            public DateTime lastReading;

            public PumpParameters()
            {
                this.PumpNumber = 0;
                this.PumpNominalPower = 0;
                this.PumpState = 0;

                this.SumPower = 0;
                this.ActPower = 0;

                this.Voltage_1 = String.Empty;
                this.Voltage_2 = String.Empty;
                this.Voltage_3 = String.Empty;

                this.Current_1 = String.Empty;
                this.Current_2 = String.Empty;
                this.Current_3 = String.Empty;

                this.Power_1 = String.Empty;
                this.Power_2 = String.Empty;
                this.Power_3 = String.Empty;

                this.WorkinTime = 0;
            }

            public void SetActivePower(double ActivePower)
            {
                this.ActPower = ActivePower;
            }

            public void SetWorkTime(long WorkTime)
            {
                this.WorkinTime = WorkTime;
            }

            public void SetSummaryPower(double SummaryPower)
            {
                this.SumPower = SummaryPower;
            }
        }

        public static DateTime GetNetworkTime()
        {
            try
            {
                DateTime dateTime = DateTime.MinValue;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://nist.time.gov/actualtime.cgi?lzbc=siqm9b");
                request.Method = "GET";
                request.Accept = "text/html, application/xhtml+xml, */*";
                request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore); //No caching
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader stream = new StreamReader(response.GetResponseStream());
                    string html = stream.ReadToEnd();//<timestamp time=\"1395772696469995\" delay=\"1395772696469995\"/>
                    string time = Regex.Match(html, @"(?<=\btime="")[^""]*").Value;
                    double milliseconds = Convert.ToInt64(time) / 1000.0;
                    dateTime = new DateTime(1970, 1, 1).AddMilliseconds(milliseconds).ToLocalTime();
                }

                return dateTime;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return DateTime.Now;
            }
        }

        public static string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = 0;

                                decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        public static void UpdateDB(int obisCode, int parameterNumber, long value)
        {
            string cs = @"server=localhost;userid=root;
                      password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                {
                    string sql = "Update GetParameters Set Value=" + value + " Where obisCode = " + obisCode + " AND parameterNumber = " + parameterNumber;

                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    cmd.Prepare();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " fuinction UpdateDB()");
                Thread.Sleep(500);
            }
            Thread.Sleep(500);
        }

        public static long GetValueFromDB(int obisCode, int parameterNumber)
        {
            string cs = @"server=localhost;userid=root;
                      password=raspberry;database=RaspDB";

            long value = 0;

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                {
                    string sql = "SELECT Value from GetParameters Where obisCode = " + obisCode + " AND parameterNumber = " + parameterNumber;


                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        value = Convert.ToInt64(rdr.GetString(0));
                    }

                    conn.Close();
                    Thread.Sleep(500);
                }
                return value;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " fuinction UpdateDB()");
                return 0;
            }
        }

        public static void WriteToLog(String message)
        {
            try
            {
                lock (logsLocker)
                {
                    File.AppendAllText("/usr/local/bin/Logs.txt", " >>> " + DateTime.Now.ToString() + " : " + message + Environment.NewLine);
                    FileInfo f = new FileInfo("/usr/local/bin/Logs.txt");
                    if (f.Length > 10000000) File.Delete("/usr/local/bin/Logs.txt"); // if File is greater than 10 MB
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function WriteToLog()"); }
        }

        static public List<string> ReadFileLineByLine(string name)
        {
            List<string> returnValue = new List<string>();

            try
            {
                String line = String.Empty;
                StreamReader file = new StreamReader(name);

                while ((line = file.ReadLine()) != null)
                {
                    while (line != "<conf>")
                    {
                        line = line.Replace("\'", "");
                        if (line.Contains("StationID")) Station = line.Replace("StationID = ", "");
                        if (line.Contains("RaspberryID")) RaspberryID = line.Replace("RaspberryID = ", "");
                        if (line.Contains("Server")) Server = line.Replace("Server = ", "");
                        if (line.Contains("Port")) Port = line.Replace("Port = ", "");
                        line = file.ReadLine();
                    }
                    while ((line = file.ReadLine()) != "</conf>") returnValue.Add(line);
                }
                file.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function ReadFileLineByLine()");
            }
            return returnValue;
        }

        static List<string> ReplaceUnusedCharacters(List<string> replace)
        {
            for (int i = 0; i < replace.Count; i++)
            {
                replace[i] = replace[i].Replace("\'", "");
                replace[i] = replace[i].Replace("\t", "");
                replace[i] = replace[i].Replace("</", "");
                replace[i] = replace[i].Replace("<", "");
                replace[i] = replace[i].Replace(">", "");
                replace[i] = replace[i].Replace("\n", "");
                replace[i] = replace[i].Replace("\'", "");
                replace[i] = replace[i].Replace(" ", "");
                if (replace[i] == "") replace.RemoveAt(i);
            }
            return replace;
        }

        static void HDLCConvert(byte[] array)
        {
            byte[] sum = new byte[2];
            byte[] HDLC_HEADER = new byte[8];
            for (int i = 1, j = 0; i < HDLC_HEADER.Length + 1; i++, j++)
            {
                HDLC_HEADER[j] = array[i];
            }
            short crc = CrcHDLC(HDLC_HEADER, HDLC_HEADER.Length, 0);
            sum = BitConverter.GetBytes(crc);
            array[9] = sum[0];
            array[10] = sum[1];
        }

        static void CalculateArray(byte[] array)
        {
            byte[] ret = new byte[2];
            int size = array.Length - 4;
            byte[] cal = new byte[size];
            int k = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (i > 0 && i < array.Length - 3)
                {
                    cal[k] = array[i];
                    k++;
                }
            }
            short crc = CrcHDLC(cal, cal.Length, 0);//calculate_CRC16(cal, cal.Length);
            ret = BitConverter.GetBytes(crc);
            Array.Reverse(ret);
            array[array.Length - 2] = ret[0];
            array[array.Length - 3] = ret[1];
        }

        static void CalculateSum(int address)
        {
            byte[] array = BitConverter.GetBytes(address);
            Array.Reverse(array);

            for (int i = 3, j = 0; i < 7; i++, j++)
            {
                SNRM[i] = array[j];
                AARQ[i] = array[j];
                DISC[i] = array[j];
                readVoltage_A[i] = array[j];
                readVoltage_B[i] = array[j];
                readVoltage_C[i] = array[j];
                readAmper_A[i] = array[j];
                readAmper_B[i] = array[j];
                readAmper_C[i] = array[j];
                readPower_A[i] = array[j];
                readPower_B[i] = array[j];
                readPower_C[i] = array[j];
                SumPow[i] = array[j];
                AvrPow[i] = array[j];
            }

            HDLCConvert(AARQ);
            HDLCConvert(readVoltage_A);
            HDLCConvert(readVoltage_B);
            HDLCConvert(readVoltage_C);
            HDLCConvert(readAmper_A);
            HDLCConvert(readAmper_B);
            HDLCConvert(readAmper_C);
            HDLCConvert(readPower_A);
            HDLCConvert(readPower_B);
            HDLCConvert(readPower_C);
            HDLCConvert(AvrPow);
            HDLCConvert(SumPow);
            CalculateArray(SNRM);
            CalculateArray(AARQ);
            CalculateArray(DISC);
            CalculateArray(readVoltage_A);
            CalculateArray(readVoltage_B);
            CalculateArray(readVoltage_C);
            CalculateArray(readAmper_A);
            CalculateArray(readAmper_B);
            CalculateArray(readAmper_C);
            CalculateArray(readPower_A);
            CalculateArray(readPower_B);
            CalculateArray(readPower_C);
            CalculateArray(AvrPow);
            CalculateArray(SumPow);
        }

        public static short CrcHDLC(byte[] data, int size, int iOffset)
        {
            int wCrc;
            byte bTmp;

            wCrc = (int)0xFFFF;//InitHdlcCrc;
            for (int i = 0; i < size; i++)
            {
                bTmp = (byte)(data[iOffset + i] ^ (byte)(wCrc & 0x00ff));
                bTmp = (byte)((bTmp << 4) ^ bTmp);
                wCrc = (int)(wCrc >> 8);

                int iTmp0 = bTmp << 8;
                iTmp0 ^= (bTmp << 3) & 0x07ff;
                iTmp0 ^= (bTmp >> 4) & 0x0000000f;

                wCrc ^= iTmp0 & 0x0000ffff;
            }
            wCrc ^= 0xffff;

            return (short)wCrc;
        }

        static byte[] ReadPacket(SerialPort port)
        {
            int byteCount = port.BytesToRead;
            byte[] retBytes = new byte[byteCount];
            port.Read(retBytes, 0, byteCount);
            return retBytes;
        }

        public static void CalculatePumpsParameters(NIK nik)
        {
            try
            {
                DateTime current = GetNetworkTime();

                double totalNominalPowerWorking = 0, totalNominalPower = 0;
                int pumpsCount = PumpsPower.Count, workingPumps = 0;

                for (int i = 0; i < PumpsPower.Count; i++)
                {
                    parameters[i].PumpState = GPIOPumps[i].GPIOGetValue(); // get pump state
                    parameters[i].PumpNominalPower = PumpsPower[i]; // get pump nominal power
                    parameters[i].PumpNumber = i + 1; // get pump number
                    parameters[i].WorkinTime = GetValueFromDB(10, i + 1); // get pump work time from DB

                    totalNominalPower = totalNominalPower + PumpsPower[i]; // find sum of nominal powers

                    if (parameters[i].PumpState == 1)
                    {
                        workingPumps = workingPumps + 1; // find count of pumps in work
                        totalNominalPowerWorking = totalNominalPowerWorking + PumpsPower[i]; // find sum of nominal powers of WORKING pumps

                        TimeSpan time = current - parameters[i].lastReading; // calculate time difference
                        parameters[i].WorkinTime += parameters[i].WorkinTime + Convert.ToInt32(time.TotalMilliseconds); // add value to structure
                        UpdateDB(10, parameters[i].PumpNumber, parameters[i].WorkinTime); // write this value to DB
                        parameters[i].lastReading = current; // get time of last reading
                    }
                    else // if pump is not active
                    {
                        parameters[i].lastReading = current;
                        parameters[i].Power_1 = "0";
                        parameters[i].Power_2 = "0";
                        parameters[i].Power_3 = "0";
                        parameters[i].Current_1 = "0";
                        parameters[i].Current_2 = "0";
                        parameters[i].Current_3 = "0";
                        parameters[i].ActPower = 0;
                    }
                }

                for (int i = 0; i < PumpsPower.Count; i++)
                {
                    Console.WriteLine("==============================");
                    if (parameters[i].PumpState == 1)
                    {
                        parameters[i].coefficient = parameters[i].PumpNominalPower / totalNominalPowerWorking;
                        Console.WriteLine(">>> Coefficient = " + parameters[i].coefficient);
                        parameters[i].ActPower = parameters[i].coefficient * Convert.ToDouble(nik.ActPower);
                        parameters[i].Current_1 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_1));
                        parameters[i].Current_2 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_2));
                        parameters[i].Current_3 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Current_3));
                        parameters[i].Power_1 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_1));
                        parameters[i].Power_2 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_2));
                        parameters[i].Power_3 = Convert.ToString(parameters[i].coefficient * Convert.ToDouble(nik.Power_3));
                    }

                    Console.WriteLine(">>> Pump Number : " + parameters[i].PumpNumber);
                    Console.WriteLine(">>> Pump State : " + parameters[i].PumpState);
                    Console.WriteLine(">>> Pump Nominal Power : " + parameters[i].PumpNominalPower);
                    Console.WriteLine(">>> Pump Active Power : " + parameters[i].ActPower);
                    Console.WriteLine(">>> WorkTime Power : " + parameters[i].WorkinTime);
                    Console.WriteLine("==============================");
                }
                PumpToXmlFormat(parameters);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function CalculatePumpsParameters()");
            }
        }

        public static void PumpToXmlFormat(List<PumpParameters> lisToSend)
        {
            foreach (var a in lisToSend)
            {
                using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/Pump.xml", System.Text.Encoding.UTF8))
                {
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartElement("Pump");
                    writer.WriteStartElement("Station");
                    writer.WriteString(Convert.ToString(Station));
                    writer.WriteEndElement();
                    writer.WriteStartElement("PumpNumber");
                    writer.WriteString(Convert.ToString(a.PumpNumber));
                    writer.WriteEndElement();
                    writer.WriteStartElement("SummaryPower");
                    writer.WriteString(Convert.ToString(a.SumPower));
                    writer.WriteEndElement();
                    writer.WriteStartElement("ActivePower");
                    writer.WriteString(Convert.ToString(a.ActPower));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper1");
                    writer.WriteString(Convert.ToString(a.Current_1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper2");
                    writer.WriteString(Convert.ToString(a.Current_2));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper3");
                    writer.WriteString(Convert.ToString(a.Current_3));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power1");
                    writer.WriteString(Convert.ToString(a.Power_1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power2");
                    writer.WriteString(Convert.ToString(a.Power_2));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power3");
                    writer.WriteString(Convert.ToString(a.Power_3));
                    writer.WriteEndElement();
                    writer.WriteStartElement("WorkingTime");
                    writer.WriteString(Convert.ToString(a.WorkinTime));
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    String sendString = File.ReadAllText("/usr/local/bin/Pump.xml");
                    File.Delete("/usr/local/bin/Pump.xml");
                    Send(sendString);
                }
            }
        }

        public static void Nik(String name, String addr, String Station, int setFrequency)
        {
            byte[] receive = new byte[64];
            int number = 0, delayTime = 1000; ;

            try
            {
                addr = addr.Replace("Address=", "");
                int address = Convert.ToInt32(addr);
                Console.WriteLine(">>> Device  - " + name + "- Adress : " + address + " station - : " + Station);
                ushort high = 0, low = 0;
                BarcodeToAddr(address, ref high, ref low);
                high = HDLC_ADDRESS_CONVERT(high, 0);
                low = HDLC_ADDRESS_CONVERT(low, 1);

                address = (high << 16) | low;
                CalculateSum(address);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function Nik()");
            }

            while (true)
            {
                if (Array.IndexOf(SerialPort.GetPortNames(), "/dev/ttyUSB0") > -1)
                {
                    try
                    {
                        port = new SerialPort("/dev/ttyUSB0", 9600, Parity.None, 8, StopBits.One);

                        port.Handshake = Handshake.None;
                        port.ReadTimeout = 2000;
                        port.WriteTimeout = 2000;

                        port.Open();

                        port.Write(SNRM, 0, SNRM.Length); //Sending frame SNRM
                        Thread.Sleep(delayTime);
                        receive = ReadPacket(port); //Answer from server UA

                        port.Write(AARQ, 0, AARQ.Length); //Sending frame AARQ
                        Thread.Sleep(delayTime);
                        receive = ReadPacket(port); //Answer from server AARE

                        NIK nik = new NIK(); // Read parameters from NIK

                        nik.Voltage_1 = GetParam(port, readVoltage_A, 20, 29, delayTime);
                        nik.Voltage_2 = GetParam(port, readVoltage_B, 20, 29, delayTime);
                        nik.Voltage_3 = GetParam(port, readVoltage_C, 20, 29, delayTime);

                        nik.Current_1 = GetParam(port, readAmper_A, 20, 29, delayTime);
                        nik.Current_2 = GetParam(port, readAmper_B, 20, 29, delayTime);
                        nik.Current_3 = GetParam(port, readAmper_C, 20, 29, delayTime);

                        nik.Power_1 = GetParam(port, readPower_A, 20, 29, delayTime);
                        nik.Power_2 = GetParam(port, readPower_B, 20, 29, delayTime);
                        nik.Power_3 = GetParam(port, readPower_C, 20, 29, delayTime);

                        nik.SumPower = GetParam(port, SumPow, 20, 29, delayTime);
                        nik.ActPower = GetParam(port, AvrPow, 20, 29, delayTime);

                        port.Write(DISC, 0, DISC.Length); //Sending AARQ request
                        Thread.Sleep(delayTime);

                        receive = ReadPacket(port); // Receive succsess request
                        port.Close();

                        DateTime current = GetNetworkTime();
                        if (Convert.ToDouble(nik.ActPower) > 0.3)
                        {

                            TimeSpan time = current - previous;
                            totalTime = totalTime + Convert.ToInt64(time.TotalMilliseconds);
                            nik.WorkinTime = totalTime;
                            previous = current;
                        }
                        else previous = current;

                        if (PumpsPower.Count > 0)
                        {
                            CalculatePumpsParameters(nik);
                        }
                        else
                        {
                            String sendString = WriteToXml(nik, 0, 0);
                            //Console.WriteLine(sendString);
                            Send(sendString);
                        }

                        number = 0;
                        Thread.Sleep(TimeSpan.FromSeconds(setFrequency));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + " function Nik()");

                        lock (errorLocker)
                        {
                            ErrorWord = 5; // Error with nik and serial interface
                        }

                        setFrequency += 5; //increase time to next interview
                        delayTime += 100; // increase time between requests

                        Thread.Sleep(TimeSpan.FromSeconds(10));
                        if (++number >= 20)
                        {
                            System.Environment.Exit(-1);
                        }
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("COM rs485 not found / counter : " + (number + 1));

                    lock (errorLocker)
                    {
                        ErrorWord = 5; // Error with nik and serial interface
                    }

                    if (++number >= 20) System.Environment.Exit(-1);
                    Thread.Sleep(5000);
                }
            }
        }

        private static string WriteToXml(NIK nik, int pumpCount, int pumpNumber = 0)
        {
            string ret = String.Empty;

            try
            {
                using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/NIK.xml", System.Text.Encoding.UTF8))
                {
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartElement("NIK");
                    writer.WriteStartElement("Station");
                    writer.WriteString(Convert.ToString(Station));
                    writer.WriteEndElement();
                    writer.WriteStartElement("PumpCount");
                    writer.WriteString(Convert.ToString(pumpCount));
                    writer.WriteEndElement();
                    writer.WriteStartElement("WorkingPump");
                    writer.WriteString(Convert.ToString(pumpNumber));
                    writer.WriteEndElement();
                    writer.WriteStartElement("SummaryPower");
                    writer.WriteString(Convert.ToString(nik.SumPower));
                    writer.WriteEndElement();
                    writer.WriteStartElement("ActivePower");
                    writer.WriteString(Convert.ToString(nik.ActPower));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Voltage1");
                    writer.WriteString(Convert.ToString(nik.Voltage_1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Voltage2");
                    writer.WriteString(Convert.ToString(nik.Voltage_2));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Voltage3");
                    writer.WriteString(Convert.ToString(nik.Voltage_3));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper1");
                    writer.WriteString(Convert.ToString(nik.Current_1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper2");
                    writer.WriteString(Convert.ToString(nik.Current_2));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Amper3");
                    writer.WriteString(Convert.ToString(nik.Current_3));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power1");
                    writer.WriteString(Convert.ToString(nik.Power_1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power2");
                    writer.WriteString(Convert.ToString(nik.Power_2));
                    writer.WriteEndElement();
                    writer.WriteStartElement("Power3");
                    writer.WriteString(Convert.ToString(nik.Power_3));
                    writer.WriteEndElement();
                    writer.WriteStartElement("WorkingTime");
                    writer.WriteString(Convert.ToString(nik.WorkinTime));
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    ret = File.ReadAllText("/usr/local/bin/NIK.xml");
                    File.Delete("/usr/local/bin/NIK.xml");
                    return ret;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function WriteToXml(Nik nik)"); return null; }
        }

        static string GetParam(SerialPort port, byte[] request, int first, int last, int delayTime)
        {
            string g = String.Empty;
            try
            {
                byte[] receive = new byte[64];
                port.Write(request, 0, request.Length); //Sending frame DISC
                Thread.Sleep(delayTime);  //maybe 200

                receive = ReadPacket(port);

                for (int i = first; i < last; i++)
                {
                    g += Convert.ToChar(receive[i]);
                }
                Console.WriteLine(">>> " + g);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function nik GetParam()"); }
            return g;
        }

        static void BarcodeToAddr(long bar, ref ushort addrHI, ref ushort addrLO)
        {
            addrHI = 0;
            addrLO = (ushort)(bar & 0x3FFF);
            if ((bar & 0x3FFF) < 16)
            {
                addrLO += 16;
                addrHI |= (1 << 12);
            }
            else
            {
                addrLO = (ushort)(bar & 0x3FFF);
            }
            if (((bar >> 14) & 0x3FFF) < 16)
            {
                addrHI += (ushort)(((bar >> 14) & 0x3FFF) + 16);
                addrHI |= (1 << 13);
            }
            else
            {
                addrHI |= (ushort)((bar >> 14) & 0x3FFF);
            }
        }

        static ushort HDLC_ADDRESS_CONVERT(int a, int l)
        {
            return (ushort)(((((a) << 1) & 0x00FE) | (l)) | (((a) << 2) & 0xFE00));
        }

        static ushort HDLC_ADDRESS_DECODE(int a)
        {
            return (ushort)((((a) >> 1) & 0x007F) | (((a) >> 2) & 0x3F80));
        }

        public static String DeviceXML(String name, String pin, String Station, String ID, Double Value)
        {
            try
            {
                using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/" + name + pin + ".xml", System.Text.Encoding.UTF8))
                {
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartElement(name);
                    writer.WriteStartElement("Station");
                    writer.WriteString(Station);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ID");
                    writer.WriteString(ID);
                    writer.WriteEndElement();
                    writer.WriteStartElement("Value");
                    writer.WriteString(Convert.ToString(Value));
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                }
                string xml = File.ReadAllText("/usr/local/bin/" + name + pin + ".xml");
                Thread.Sleep(500);
                File.Delete("/usr/local/bin/" + name + pin + ".xml");
                return xml;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function DeviceXml()");
                return String.Empty;
            }
        }

        public static void Send(string xml)
        {
            TcpClient client = new TcpClient();
            xml = Encrypt(xml, key);
            byte[] data = Encoding.ASCII.GetBytes(xml);

            try
            {
                client = new TcpClient(Server, Convert.ToInt32(Port));

                client.SendTimeout = 5000;
                client.ReceiveTimeout = 5000;

                Console.WriteLine(">>> Trying to send data " + Server + " - " + Port);

                Socket socket = client.Client;
                socket.Send(data);

                client.Client.Shutdown(SocketShutdown.Both);
                client.Client.Disconnect(false);
                client.Client.Close();
                client.Close();
                count = 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function Send()");

                try
                {
                    client.Client.Shutdown(SocketShutdown.Both);
                    client.Client.Disconnect(false);
                    client.Client.Close();
                    client.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                lock (errorLocker)
                {
                    ErrorWord = 4; // Error with network
                }

                if (++count >= 20)
                {
                    System.Environment.Exit(-1);
                }
            }
        }

        public static void ADC(String name, String ID, String Station, int setFrequency)
        {
            Console.WriteLine(">>> Device - " + name + " ID - : " + ID + " Station - : " + Station);

            String xml = String.Empty;
            double value = 0;

            while (true)
            {
                try
                {
                    var info = new ProcessStartInfo();
                    info.FileName = "sudo";
                    info.Arguments = "/usr/local/bin/i2c";

                    info.UseShellExecute = false;
                    info.CreateNoWindow = true;

                    info.RedirectStandardOutput = true;
                    info.RedirectStandardError = true;

                    var p = Process.Start(info);

                    while (!p.StandardOutput.EndOfStream)
                    {
                        string err = p.StandardOutput.ReadLine();
                        if (err.Contains("Result"))
                        {
                            err = err.Replace("Result", "");
                            value = Convert.ToDouble(err);
                        }
                        Console.WriteLine(err);
                    }
                    p.WaitForExit();

                    if (p.ExitCode != 0)
                    {
                        Console.WriteLine("Problems with i2c Driver!");
                        Thread.Sleep(TimeSpan.FromSeconds(5));
                        setFrequency += 1;
                        continue;
                    }

                    Console.WriteLine(value);
                    xml = DeviceXML(name, "0", Station, ID, value);
                    Send(xml);
                    Thread.Sleep(TimeSpan.FromSeconds(setFrequency));
                }
                catch (Exception ex)
                {
                    setFrequency += 1;
                    Console.WriteLine(ex.Message + " function ADC()");
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    continue;
                }
            }
        }

        public static Int32 GetCounter()
        {
            //Console.WriteLine("Start getCounter()");
            int counter = 0, lastState = 0, currentState = 0;
            int watchDogTimer = 0, BitIndex = 0, BitData = 0;

            GPIO gpioCLK = Export("24", "in");
            GPIO gpioData = Export("10", "in");

            lastState = gpioCLK.GPIOGetValue();

            while (true)
            {
                currentState = gpioCLK.GPIOGetValue();

                if (lastState != currentState)
                {
                    watchDogTimer = 0;
                    if (currentState == 1)
                    {
                        Thread.Sleep(10);

                        BitData = gpioData.GPIOGetValue();

                        if (BitData == 1)
                        {
                            counter |= (1 << BitIndex);
                        }

                        if (BitIndex < 31) BitIndex = BitIndex + 1;
                        else return counter;
                    }
                    lastState = currentState;
                }
                else
                {
                    watchDogTimer = watchDogTimer + 1;

                    if (watchDogTimer >= 50)
                    {
                        watchDogTimer = 0;
                        BitIndex = 0;
                        counter = 0;
                    }
                    Thread.Sleep(10);
                }
            }
        }

        public static void ReadCounter(String name, String pin, String ID, String Station, long counter, int setFrequency)
        {
            try
            {
                Console.WriteLine(">>> Device - " + name + " pin - : " + pin + " ID - : " + ID + " station - : " + Station);

                Thread sendImpulses = new Thread(unused => sendValue(name, pin, ID, Station, ref counter, setFrequency));
                sendImpulses.IsBackground = true;
                sendImpulses.Start();

                while (true)
                {
                    counter = GetCounter();
                    Console.WriteLine(">>> Device - Impulse counter = " + counter);
                    Thread.Sleep(setFrequency);
                }
                //SerialPort port = new SerialPort("/dev/ttyAMA0", 9600, Parity.None, 8, StopBits.One);

                //port.ReadTimeout = 5000;
                //port.WriteTimeout = 5000;


                //if (!(port.IsOpen)) port.Open();

                //port.DiscardOutBuffer();
                //port.DiscardInBuffer();

                //GPIO gpio = Export("24", "out");

                //gpio.GPIOSetValue("1");
                //Thread.Sleep(15000);

                //byte[] recBytes = ReadPacket(port);

                //Console.WriteLine("AMA 0 is readed succecfully");

                //String message = Encoding.ASCII.GetString(recBytes);
                //string[] number = message.Split(' ');
                //List<String> numberList = new List<string>(number);
                //List<String> duplicates = numberList.GroupBy(x => x).Where(g => g.Count() > 2).Select(g => g.Key).ToList();

                //foreach (var a in duplicates)
                //{
                //    Console.WriteLine("Getted several values " + a);
                //}

                //gpio.GPIOSetValue("0");
                //Thread.Sleep(setFrequency);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function ReadCounter()");
                //if (port.IsOpen) port.Close();
                Thread.Sleep(10000);
            }
        }

        public static void pulseCounter(String name, String pin, String ID, String Station, long counter, int setFrequency)
        {
            Console.WriteLine(">>> Device - " + name + " pin - : " + pin + " ID - : " + ID + " station - : " + Station);

            Thread sendImpulses = new Thread(unused => sendValue(name, pin, ID, Station, ref counter, setFrequency));
            sendImpulses.IsBackground = true;
            sendImpulses.Start();

            int lastIndex = 0, current = 0, counter1 = 0;

            GPIO gpio = Export(pin, "in");

            while (true)
            {
                if (counter1 > 10000)
                {
                    DateTime dt = DateTime.Now;
                    Console.WriteLine(dt.ToLongTimeString() + " : Counter = " + counter);
                    counter1 = 0;
                }
                else counter1++;

                try
                {
                    current = gpio.GPIOGetValue();
                    if (lastIndex == 0 && current == 1)
                    {
                        lastIndex = current;
                    }
                    if (lastIndex == 1 && current == 0)
                    {
                        counter++;
                        lastIndex = current;
                    }
                    Thread.Sleep(1);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " function pulseCounter()");
                    setFrequency += 1;

                    lock (errorLocker)
                    {
                        ErrorWord = 1; // Error with pulse counter
                    }

                    if (++count >= 20)
                    {
                        System.Environment.Exit(-1);
                    }
                    continue;
                }
            }
        }

        public static void Optron(String name, String pin, string ID, string Station, int setFrequency)
        {
            Console.WriteLine(">>> Device - " + name + " pin - : " + pin + " ID - : " + ID + " station - : " + Station);

            long value = 0;

            Thread sendValueThread = new Thread(unused => sendValue(name, pin, ID, Station, ref value, setFrequency));
            sendValueThread.IsBackground = true;
            sendValueThread.Start();

            while (true)
            {
                try
                {
                    value = Convert.ToInt32(Optron(pin));

                    Thread.Sleep(1);
                    count = 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " function Optron()");
                    setFrequency += 1;

                    if (name.Contains("Starter"))
                    {
                        lock (dbLocker)
                        {
                            UpdateDB(78, 0, value);
                        }
                    }

                    lock (errorLocker)
                    {
                        if (name.Contains("moveSensor")) ErrorWord = 2;
                        if (name.Contains("Starter")) ErrorWord = 3;
                    }

                    if (++count >= 20)
                    {
                        System.Environment.Exit(-1);
                    }
                }
            }
        }

        public static void sendValue(string name, string pin, string ID, string Station, ref long value, int setFrequency)
        {
            String xml = String.Empty;

            while (true)
            {
                long lastVal = value;

                try
                {
                    xml = DeviceXML(name, pin, Station, ID, Convert.ToDouble(value));

                    if (name.Contains("pulseCounter")) UpdateDB(65, 0, value);
                    if (name.Contains("Starter")) UpdateDB(78, 0, value);
                    if (name.Contains("ErrorCode")) UpdateDB(48, 0, value);

                    Console.WriteLine(">>> Send function - Send " + name + "packet, value = " + value);
                    if (xml != String.Empty) Send(xml);
                    Thread.Sleep(TimeSpan.FromSeconds(setFrequency));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " function SendValue()");
                    Thread.Sleep(10000);
                    setFrequency += 1;
                    continue;
                }
            }
        }

        public static void outStart(GPIO gpio, bool val)
        {
            try
            {
                int state = (val == true ? 1 : 0);
                gpio.GPIOSetValue(Convert.ToString(state));
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function outStart()"); }
        }

        public static void outStop(GPIO gpio, bool val)
        {
            try
            {
                int state = (val == true ? 1 : 0);
                gpio.GPIOSetValue(Convert.ToString(state));
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function outStop()"); }
        }

        public static bool inStart()
        {
            bool retVal = (ReleOneState == 1 ? true : false);
            return retVal;
        }

        public static bool inStop()
        {
            bool retVal = (ReleTwoState == 1 ? true : false);
            return retVal;
        }

        public static bool Optron(String pin)
        {
            GPIO gpio = new GPIO(pin);

            int timer = 20, lastState = 1, currentState = 0, state = 0;

            currentState = gpio.GPIOGetValue();
            lastState = currentState;

            try
            {
                while (timer > 0)
                {
                    currentState = gpio.GPIOGetValue();
                    if (lastState != currentState) state = 1;
                    timer--;
                    Thread.Sleep(10);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + " function Optron()"); }
            bool retVal = (state == 1 ? true : false);
            return retVal;
        }

        public static string CreateRelesRequestString(string ParameterID1, string ParameterID2)
        {
            try
            {
                using (XmlTextWriter writer = new XmlTextWriter("/usr/local/bin/Reles.xml", System.Text.Encoding.UTF8))
                {
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartElement("Reles");
                    writer.WriteStartElement("Station");
                    writer.WriteString(Station);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ParameterID1");
                    writer.WriteString(ParameterID1);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ParameterID2");
                    writer.WriteString(ParameterID2);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                }
                string xml = File.ReadAllText("/usr/local/bin/Reles.xml");
                File.Delete("/usr/local/bin/Reles.xml");
                return xml;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function CreateRelesRequestString()");
                return String.Empty;
            }
        }

        public static void PullUpResistors()
        {
            try
            {
                var info = new ProcessStartInfo();
                info.FileName = "sudo";
                info.Arguments = "/usr/local/bin/Up";

                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                info.RedirectStandardOutput = true;
                info.RedirectStandardError = true;

                var p = Process.Start(info);
                p.WaitForExit();
                //Console.WriteLine("Up");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + "function PullUpResistors()"); }
        }

        public static GPIO Export(string pin, string direction)
        {
            GPIO gpio = new GPIO(pin);

            try
            {
                gpio.GPIOExport();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " function Export()");
                try
                {
                    Thread.Sleep(500);
                    gpio.GPIOUnexport();
                    Thread.Sleep(500);
                    gpio.GPIOExport();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + " function Export()");
                    Console.WriteLine(">>> Device GPIO - Export failed");
                }
            }
            gpio.GPIOSetDirection(direction);
            return gpio;
        }

        public static bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0); // detect for socket connection
            }
            catch (SocketException) { return false; }
        }

        public static void ReleWork(String ParameterID_ReleOne, String ParameterID_ReleTwo)
        {
            TcpClient client;
            NetworkStream stream;

            bool b_Stop = false;
            bool b_Start = false;
            bool Everything_OK = true; // переменная все_ок
            byte Timer_Ticks = 2; // 1c
            byte Last_State = 0;
            bool Interlock_Status = true; // True - OK 
            byte btState = 0;
            bool Raspberry_Ready; // готовность малинки
            bool bIN_Start = false;
            bool bIN_Stop = false;
            bool Raspberry_Status = false;
            bool bEN;
            bool bIN_Feedback;
            int iTimer_Start = 0;
            int iTimer_Stop = 0;

            Console.WriteLine(">>> Reles function - Last current state = " + LastCurrentState);
            if (LastCurrentState == 1) Last_State = 2;

            bool First_Init = true;

            // Connect to server and wait changes in DB
            while (true)
            {
                try
                {
                    client = new TcpClient(Server, Convert.ToInt32(Port));
                    stream = new NetworkStream(client.Client);

                    stream.ReadTimeout = 5000;
                    stream.WriteTimeout = 5000;

                    String xml = String.Empty;
                    xml = CreateRelesRequestString(ParameterID_ReleOne, ParameterID_ReleTwo);
                    xml = Encrypt(xml, key);
                    stream.Write(Encoding.ASCII.GetBytes(xml), 0, Encoding.ASCII.GetBytes(xml).Length);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " function ReleWork() - connect to server part");
                    errorCounter = errorCounter + 1;
                    if (errorCounter >= 20) System.Environment.Exit(-1);
                    Thread.Sleep(5000);
                    continue;
                }

                while (true) // waiting for changes
                {
                    try
                    {
                        errorCounter = 0;
                        if (First_Init == true)
                        {
                            First_Init = false;
                            btState = 0;
                        }

                        // Code for not sending request to server, but server send result if value in DB was changed
                        if (!(IsConnected(client.Client)))
                        {
                            try
                            {
                                stream.Close();
                                client.Client.Shutdown(SocketShutdown.Both);
                                client.Client.Disconnect(true);
                                client.Client.Close();
                                client.Close();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message + " function ReleWork() problems with socket");
                            }
                            throw new Exception("Socket is not connected!");
                        }

                        if (stream.DataAvailable)
                        {
                            Console.WriteLine(">>> Reles control - Stream data available");

                            byte[] receive = new byte[2];
                            int bytesRead = stream.Read(receive, 0, receive.Length);

                            receive = receive.Take(bytesRead).ToArray();

                            ReleOneState = Convert.ToInt32(receive[0]);
                            ReleTwoState = Convert.ToInt32(receive[1]);
                        }

                        bIN_Start = Convert.ToBoolean(ReleOneState);
                        bIN_Stop = Convert.ToBoolean(ReleTwoState);
                        bIN_Feedback = Optron(starterPin);
                        Thread.Sleep(1000);

                        Console.WriteLine(">>> Start command state = " + bIN_Start);
                        Console.WriteLine(">>> Stop command state = " + bIN_Stop);
                        Console.WriteLine(">>> Starter Feedback state = " + bIN_Feedback);

                        if (bIN_Start == true) // отслеживаем верхний фронт сигнала пуска
                        {
                            b_Start = true;
                            // Reset(ParameterID_ReleOne);
                        }
                        else bIN_Start = false;

                        if (bIN_Stop == true) // отслеживаем верхний фронт сигнала стопа
                        {
                            b_Stop = true;
                            //Reset(ParameterID_ReleTwo);
                        }
                        else bIN_Stop = false;

                        switch (btState)
                        {
                            case 0:  // РЎРѕСЃС‚РѕСЏРЅРёРµ РРЅРёС†РёР°Р»РёР·Р°С†РёСЏ
                                {
                                    Console.WriteLine(">>> State 0");
                                    Raspberry_Status = false;
                                    bEN = false;
                                    bIN_Feedback = false;
                                    b_Start = false;
                                    b_Stop = false;
                                    outStart(gpioReleOne, false);
                                    outStop(gpioReleTwo, false);

                                    iTimer_Start = Timer_Ticks; // 10 С‚РёРєРѕРІ СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓРµС‚ 1 СЃРµРєСѓРЅРґРµ
                                    iTimer_Stop = Timer_Ticks; // 10 С‚РёРєРѕРІ СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓРµС‚ 1 СЃРµРєСѓРЅРґРµ
                                    btState = 1; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї
                                    if (LastCurrentState == 1) btState = 2;
                                    Last_State = 0;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РРЅРёС†РёР°Р»РёР·Р°С†РёСЏ РљРѕРЅРµС†
                                    break;
                                }

                            case 1:  // РЎРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї
                                {
                                    Console.WriteLine(">>> State 1");
                                    b_Stop = false;
                                    Raspberry_Status = false; // СѓР·РµР» РІС‹РєР»СЋС‡РµРЅ
                                    Raspberry_Ready = true;

                                    bEN = true; // СЂР°Р·СЂРµС€РёС‚СЊ Р·Р°РїСѓСЃРє 

                                    Last_State = 1;

                                    if ((b_Start == true) && // РЅР°Р¶Р°С‚Р° РєРЅРѕРїРєР° СЃС‚Р°СЂС‚ РІ СЃРєР°РґРµ

                                     (bEN == true)) // РІСЃРµ РІ РїРѕСЂСЏРґРєРµ
                                    {
                                        btState = 2; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° Р·Р°РїСѓСЃРєР°
                                    }

                                    if (bIN_Feedback == true) // РµСЃР»Рё РІРєР»СЋС‡РёР»СЃСЏ РєРѕРЅС‚Р°РєС‚РѕСЂ
                                    {
                                        btState = 4; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅР°СЏ СЂР°Р±РѕС‚Р°
                                        Last_State = 1;
                                    }
                                    break;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї РљРѕРЅРµС†
                                }

                            case 2:  // РЎРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° Р·Р°РїСѓСЃРєР°
                                {
                                    Console.WriteLine(">>> State 2");

                                    outStart(gpioReleOne, true); // РІРєР»СЋС‡РёС‚СЊ РєРѕРЅС‚Р°РєС‚РѕСЂ 
                                    if (iTimer_Start == 0) // РµСЃР»Рё С‚Р°Р№РјРµСЂ РёСЃС‚РµРє
                                    {

                                        btState = 3; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° Р·Р°РїСѓСЃРєР°
                                        Last_State = 2;
                                    }
                                    else iTimer_Start = iTimer_Start - 1; // СѓРјРµРЅСЊС€Р°РµРј СЃС‡РµС‚С‡РёРє

                                    break;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° Р·Р°РїСѓСЃРєР° РљРѕРЅРµС†
                                }

                            case 3: // РЎРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° Р·Р°РїСѓСЃРєР°
                                {
                                    Console.WriteLine(">>> State 3");
                                    iTimer_Start = Timer_Ticks; // РІРѕСЃСЃС‚Р°РЅРѕРІРёРј С‚Р°Р№РјРµСЂ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK
                                    Everything_OK = true;

                                    if (bIN_Feedback == false)  // РµСЃР»Рё РЅРµ Р·Р°РјРєРЅСѓР»СЃСЏ РїСѓСЃРєР°С‚РµР»СЊ
                                        Everything_OK = false;

                                    if (Everything_OK == true) // РёР»Рё Р¶Рµ СЌС‚Рѕ С„РѕСЂСЃРёСЂРѕРІР°РЅС‹Р№ СЃС‚Р°СЂС‚
                                        bEN = true; // СЂР°Р·СЂРµС€РёС‚СЊ Р·Р°РїСѓСЃРє 
                                    else
                                        bEN = false; // Р·Р°РїСѓСЃРє РЅРµ СЂР°Р·СЂРµС€РµРЅ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK РєРѕРЅРµС†
                                    // outStart(gpioReleOne, false);// СЃР±СЂРѕСЃРёС‚СЊ РєРѕРјР°РЅРґСѓ РЅР° РІРєР»СЋС‡РµРЅРёРµ, РІС‹С…РѕРґ РёРјРїСѓР»СЊСЃРЅС‹Р№, С‚РѕРµСЃС‚СЊ РєРѕРЅС‚Р°РєС‚РѕСЂ РґРѕР»Р¶РµРЅ Р±С‹Р» Р°РІС‚РѕР±Р»РѕРєРёСЂРѕРІР°С‚СЊСЃСЏ.

                                    if (Everything_OK == true)
                                    {
                                        btState = 4; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅР°СЏ СЂР°Р±РѕС‚Р°
                                        Last_State = 3;
                                    }
                                    else
                                    {
                                        btState = 5;
                                        outStart(gpioReleOne, false); // стоп насос по ошибке
                                        Last_State = 3;
                                    }
                                    break;

                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° Р·Р°РїСѓСЃРєР° РљРѕРЅРµС†
                                }

                            case 4:     // РЎРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅР°СЏ СЂР°Р±РѕС‚Р°
                                {
                                    Console.WriteLine(">>> State 4");
                                    b_Start = false;
                                    Raspberry_Status = true; // СѓР·РµР» РІРєР»СЋС‡РµРЅ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK
                                    Everything_OK = true;

                                    if (Everything_OK == true) // РёР»Рё Р¶Рµ СЌС‚Рѕ С„РѕСЂСЃРёСЂРѕРІР°РЅС‹Р№ СЃС‚Р°СЂС‚
                                        bEN = true; // СЂР°Р·СЂРµС€РёС‚СЊ Р·Р°РїСѓСЃРє 
                                    else
                                        bEN = false; // Р·Р°РїСѓСЃРє РЅРµ СЂР°Р·СЂРµС€РµРЅ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK РєРѕРЅРµС†

                                    if (bIN_Feedback == false)   // СЃР°РјРѕРїСЂРѕРёР·РІРѕР»СЊРЅРѕ РІС‹РєР»СЋС‡РёР»СЃСЏ РєРѕРЅС‚Р°РєС‚РѕСЂ
                                    {
                                        btState = 1; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї
                                        Last_State = 4;
                                    }

                                    if ((Everything_OK == true) && (b_Stop == true))
                                    {
                                        // РѕРїРµСЂР°С‚РѕСЂ РІС‹РєР»СЋС‡РёР» СЌР»РµРјРµРЅС‚, С‚СѓС‚ РµС‰Рµ РЅСѓР¶РЅРѕ РґРѕР±Р°РІРёС‚СЊ РёРЅС‚РµСЂР»РѕРє
                                        // РµС‰Рµ РЅСѓР¶РЅРѕ РїСЂРµРґСѓСЃРјРѕС‚СЂРµС‚СЊ РѕС€РёР±РєСѓ РґРІРѕР№РЅРѕРіРѕ РЅР°Р¶Р°С‚РёСЏ РєР»Р°РІРёС€, СЃС‚РѕРї Рё СЃС‚Р°СЂС‚ РѕРґРЅРѕРІСЂРµРјРµРЅРЅРѕ
                                        btState = 6; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° СЃС‚РѕРїР°
                                        Last_State = 4;
                                    }
                                    if (Everything_OK == false)
                                    {
                                        btState = 5; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ Р°РІР°СЂРёР№РЅС‹Р№ СЃС‚РѕРї
                                        outStart(gpioReleOne, false);
                                        Last_State = 4;
                                    }
                                    break;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅР°СЏ СЂР°Р±РѕС‚Р° РљРѕРЅРµС†
                                }

                            case 5: // РЎРѕСЃС‚РѕСЏРЅРёРµ Р°РІР°СЂРёР№РЅС‹Р№ СЃС‚РѕРї
                                {
                                    Console.WriteLine(">>> State 5");
                                    Raspberry_Ready = false;
                                    // С‚СѓС‚ Р±СѓРґРµС‚ РѕР±СЂР°Р±Р°С‚С‹РІР°С‚СЃСЏ Р°РІР°СЂРёСЏ.
                                    // Р° С‚Р°РєР¶Рµ РѕР¶РёРґР°РЅРёРµ РЅР° РїРµСЂРµС…РѕРґ РІ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЂРµР¶РёРј РїРѕ РєРѕРјР°РЅРґРµ РѕРїРµСЂР°С‚РѕСЂР°.
                                    Raspberry_Status = false; // СѓР·РµР» РІС‹РєР»СЋС‡РµРЅ
                                    b_Stop = false;
                                    b_Start = false;

                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK
                                    Everything_OK = true;

                                    if (Everything_OK == true)
                                    {
                                        // РѕС€РёР±РєР° СЃР±СЂРѕС€РµРЅР°
                                        outStart(gpioReleOne, false); // СЃР±СЂРѕСЃРёС‚СЊ СЃРёРіРЅР°Р» РІС‹РєР»СЋС‡РёС‚СЊ РєРѕРЅС‚Р°РєС‚РѕСЂ
                                        btState = 1; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї
                                        Last_State = 5;
                                    }
                                    break;// РЎРѕСЃС‚РѕСЏРЅРёРµ Р°РІР°СЂРёР№РЅС‹Р№ СЃС‚РѕРї РљРѕРЅРµС†
                                }

                            case 6: // РЎРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° СЃС‚РѕРїР°
                                {
                                    Console.WriteLine(">>> State 6");
                                    outStart(gpioReleOne, false); // РІС‹РєР»СЋС‡РёС‚СЊ РєРѕРЅС‚Р°РєС‚РѕСЂ 
                                    if (iTimer_Stop == 0)  // РµСЃР»Рё С‚Р°Р№РјРµСЂ РёСЃС‚РµРє
                                    {
                                        btState = 7; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° РѕСЃС‚Р°РЅРѕРІРєРё
                                        Last_State = 6;
                                    }
                                    else iTimer_Stop = iTimer_Stop - 1; // СѓРјРµРЅСЊС€Р°РµРј СЃС‡РµС‚С‡РёРє
                                    break;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РїРѕРїС‹С‚РєР° СЃС‚РѕРїР° РљРѕРЅРµС†
                                }

                            case 7: // РЎРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° СЃС‚РѕРїР°
                                {
                                    Console.WriteLine(">>> State 7");
                                    iTimer_Stop = Timer_Ticks; // РІРѕСЃСЃС‚Р°РЅРѕРІРёРј С‚Р°Р№РјРµСЂ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK
                                    Everything_OK = true;

                                    if (bIN_Feedback == true)  // РµСЃР»Рё РЅРµ СЂР°Р·РѕРјРєРЅСѓР»СЃСЏ РїСѓСЃРєР°С‚РµР»СЊ
                                        Everything_OK = false;

                                    if (Everything_OK == true) // РёР»Рё Р¶Рµ СЌС‚Рѕ С„РѕСЂСЃРёСЂРѕРІР°РЅС‹Р№ СЃС‚Р°СЂС‚
                                        bEN = true; // СЂР°Р·СЂРµС€РёС‚СЊ Р·Р°РїСѓСЃРє 
                                    else bEN = false; // Р·Р°РїСѓСЃРє РЅРµ СЂР°Р·СЂРµС€РµРЅ
                                    // Р¤СѓРЅРєС†РёСЏ Everything_OK РєРѕРЅРµС†

                                    if (Everything_OK == true)
                                    {
                                        btState = 1; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ РЅРѕСЂРјР°Р»СЊРЅС‹Р№ СЃС‚РѕРї
                                        Last_State = 7;
                                    }
                                    else
                                    {
                                        btState = 5; // РїРµСЂРµР№С‚Рё РІ СЃРѕСЃС‚РѕСЏРЅРёРµ Р°РІР°СЂРёР№РЅС‹Р№ СЃС‚РѕРї
                                        Last_State = 7;
                                    }
                                    break;
                                    // РЎРѕСЃС‚РѕСЏРЅРёРµ РїСЂРѕРІРµСЂРєР° СЃС‚РѕРїР° РљРѕРЅРµС†
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + " function ReleWork()");
                        break;
                    }
                }
            }
        }

        public static void CreateTheadsList()
        {
            try
            {
                int i = 0;
                while (i < devices.Count)
                {
                    switch (devices[i])
                    {
                        case "pulseCounter":
                            {
                                String pin = devices[i + 1].Replace("GPIO=", "");
                                String ParamID = devices[i + 2].Replace("ParameterID=", "");
                                String SampleFrequency = devices[i + 3].Replace("SamplingFrequency=", "");
                                String name = devices[i];
                                Export(pin, "in");

                                Thread pulse = new Thread(unused => ReadCounter(name, pin, ParamID, Station, counter, Convert.ToInt32(SampleFrequency)));
                                pulse.IsBackground = true;

                                ThreadList.Add(pulse);
                                i = i + 5;
                            }
                            break;
                        case "moveSensor":
                            {
                                String pin = devices[i + 1].Replace("GPIO=", "");
                                String ParamID = devices[i + 2].Replace("ParameterID=", "");
                                String SampleFrequency = devices[i + 3].Replace("SamplingFrequency=", "");
                                String name = devices[i];
                                Export(pin, "in");

                                Thread move = new Thread(unused => Optron(name, pin, ParamID, Station, Convert.ToInt32(SampleFrequency)));
                                move.IsBackground = true;

                                ThreadList.Add(move);
                                i = i + 5;
                            }
                            break;
                        case "Starter":
                            {
                                String pin = devices[i + 1].Replace("GPIO=", "");
                                String ParamID = devices[i + 2].Replace("ParameterID=", "");
                                String SampleFrequency = devices[i + 3].Replace("SamplingFrequency=", "");
                                String name = devices[i];
                                Export(pin, "in");

                                Thread starter = new Thread(unused => Optron(name, pin, ParamID, Station, Convert.ToInt32(SampleFrequency)));
                                starter.IsBackground = true;

                                ThreadList.Add(starter);
                                starterPin = pin;
                                i = i + 5;
                            }
                            break;
                        case "ADC":
                            {
                                String ParamID = devices[i + 1].Replace("ParameterID=", "");
                                String SampleFrequency = devices[i + 2].Replace("SamplingFrequency=", "");
                                String name = devices[i];

                                Thread adc = new Thread(unused => ADC(name, ParamID, Station, Convert.ToInt32(SampleFrequency)));
                                adc.IsBackground = true;

                                ThreadList.Add(adc);

                                i = i + 4;
                                Thread.Sleep(1000);
                            }
                            break;
                        case "NIK":
                            {
                                String SampleFrequency = devices[i + 2].Replace("SamplingFrequency=", "");
                                String Address = devices[i + 1].Replace("Address=", "");
                                String name = devices[i];

                                Thread nikThread = new Thread(unused => Nik(name, Address, Station, Convert.ToInt32(SampleFrequency)));
                                nikThread.IsBackground = true;

                                ThreadList.Add(nikThread);
                                i = i + 4;
                            }
                            break;
                        case "Reles":
                            {
                                String pinRele1 = devices[i + 1].Replace("ReleOnePin=", "");
                                String pinRele2 = devices[i + 2].Replace("ReleTwoPin=", "");

                                String parID_ReleOne = devices[i + 3].Replace("ParameterID_ReleOne=", "");
                                String parID_ReleTwo = devices[i + 4].Replace("ParameterID_ReleTwo=", "");

                                gpioReleOne = Export(pinRele1, "out");
                                gpioReleTwo = Export(pinRele2, "out");

                                Thread releThread = new Thread(unused => ReleWork(parID_ReleOne, parID_ReleTwo));
                                releThread.IsBackground = true;

                                ThreadList.Add(releThread);

                                i = i + 6;
                            }
                            break;
                        case "pumpsPowers":
                            {
                                i = i + 1;
                                while (devices[i] != "pumpsPowers")
                                {
                                    PumpsPower.Add(Convert.ToInt32(devices[i].Replace("Power=", "")));
                                    parameters.Add(new PumpParameters()); // add pump to global structure
                                    parameters.Last().lastReading = previous; // init time of last reading for workTime
                                    GPIOPumps.Add(Export(devices[i + 1].Replace("GPIO=", ""), "in")); // create gpio interface for getting pump state
                                    i = i + 2;
                                }
                                i = i + 1;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " CreateTrheadList()");
                System.Environment.Exit(-1);
            }
        }

        public static void Main()
        {
            try // try to read Configuration.xml
            {
                devices = ReadFileLineByLine("/usr/local/bin/Configuration.xml");
                devices = ReplaceUnusedCharacters(devices);
                Console.WriteLine("===============================================");
                Console.WriteLine(">>> Server - " + Server);
                Console.WriteLine(">>> Port - " + Port);
                Console.WriteLine(">>> StationID - " + Station);
                Console.WriteLine(">>> RaspberryID - " + RaspberryID);
                Console.WriteLine(">>> Config is reading succesfull");
                Console.WriteLine("===============================================");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " Read Configuration Error");
                System.Environment.Exit(-1);
            }

            try // Try to read values from DB and to add subsidiaries threads to ThreadList
            {
                lock (dbLocker)
                {
                    ErrorWord = (int)GetValueFromDB(48, 0);
                    LastCurrentState = (int)GetValueFromDB(78, 0);
                    counter = GetValueFromDB(65, 0);
                    totalTime = GetValueFromDB(10, 0);
                    previous = GetNetworkTime();
                }

                Console.WriteLine("===============================================");
                Console.WriteLine(">>> ErrorWord from DB = " + ErrorWord);
                Console.WriteLine(">>> StarterState from DB = " + LastCurrentState);
                Console.WriteLine(">>> Counter from DB = " + counter);
                Console.WriteLine(">>> TotalTime in milliseconds from DB = " + totalTime);
                Console.WriteLine(">>> Time initializing = " + previous.ToString());
                Console.WriteLine(">>> DataBase Values readed succesfull");
                Console.WriteLine("===============================================");
                if (ErrorWord != 0) ErrorWord = 6; // Raspberry was rebooted by overstrain

                Thread sendErrorWord = new Thread(unused => sendValue("ErrorWord", "0", "48", Station, ref ErrorWord, 30));
                sendErrorWord.IsBackground = true;
                ThreadList.Add(sendErrorWord);

                Thread up = new Thread(PullUpResistors); // Thread for Up resistors
                up.IsBackground = true;
                ThreadList.Add(up);

                CreateTheadsList(); // main Convertin of Configuration.xml

                Console.WriteLine("===============================================");
                Console.WriteLine(">>> Gpio for pumps Count : " + GPIOPumps.Count);
                Console.WriteLine(">>> Power for pumps Count : " + PumpsPower.Count);
                Console.WriteLine("===============================================");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " Runnning subsidiaries Threads Error");
            }

            foreach (var thread in ThreadList) // run all working threads
            {
                thread.Start();
                Thread.Sleep(2000);
            }

            while (true)
            {
                Thread.Sleep(200);
            }
        }
    }
}
