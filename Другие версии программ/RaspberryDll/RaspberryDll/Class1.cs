﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Xml;

namespace RaspberryDll
{
    public class Raspberry : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        private ManualResetEvent allDone = new ManualResetEvent(false);
        private object thisLock = new object();
        private const int keysize = 256;
        private readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");
        private string key = "IIltpohall";

        public Raspberry(StationEquipmentData Params) : base(Params)
        {
            StationEquipment = new StationEquipmentData(Params);
            Thread raspberry = new Thread(Start);
            raspberry.Start();
        }

        private string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        private string Decrypt(string cipherText, string passPhrase)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = 0;

                                decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }



        private void Start()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));

                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);
                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    Console.WriteLine("Waiting for a connection..."); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing.
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            Console.WriteLine("Connected");
            byte[] receive = new byte[1024];
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            try
            {
                while (true)
                {
                    Console.WriteLine("Accepted");

                    int bytesRead = handler.Receive(receive);
                    receive = receive.Take(bytesRead).ToArray();

                    if (bytesRead > 0)
                    {
                        lock (thisLock)
                        {
                            String content = Encoding.ASCII.GetString(receive, 0, bytesRead);

                            try
                            {
                                content = Decrypt(content, key);
                                Console.WriteLine(content);
                            }
                            catch (Exception ex) { Console.WriteLine(ex.ToString()); return; }

                            if (content.Contains("Reles")) ReleWork(content, handler);
                            else
                            {
                                if (content.Contains("NIK")) ReadNIK(content);
                                else if (content.Contains("Irka")) Irka(content);
                                else if (content.Contains("Reset")) Reset(content);
                                else ReadDevice(content);
                            }
                        }
                    }
                    Thread.Sleep(100);
                }
            }
            catch (Exception e) { Console.WriteLine(e.Message); handler.Disconnect(true); handler.Close(); return; }
        }

        private void Send(Socket handler, String data)
        {
            try
            {
                byte[] byteData = Encoding.ASCII.GetBytes(data);
                handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
            }
            catch (Exception ex) { handler.Shutdown(SocketShutdown.Both); }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        }

        private void Irka(string xml)
        {
            double counter = 0, momentum = 0;
            int Station = 26;
            double Address = 0;
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(xml));
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Address":
                                if (reader.Read()) Address = Convert.ToDouble(reader.Value.Trim());
                                
                                break;
                            case "Momentum":
                                if (reader.Read()) momentum = Convert.ToDouble(reader.Value.Trim());
                                Data.Add(new DataParameter(Station, 53, 0, Convert.ToDouble(momentum), StationEquipment.EquipmentTypeId));
                                break;
                            case "Counter":
                                if (reader.Read()) counter = Convert.ToDouble(reader.Value.Trim());
                                Data.Add(new DataParameter(Station, 89, 0, Convert.ToDouble(counter), StationEquipment.EquipmentTypeId));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); return; }
        }

        public void Reset(string xml)
        {
            int Station = 0, ParameterID = 0;

            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(xml));
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Station":
                                if (reader.Read()) Station = Convert.ToInt32(reader.Value.Trim());
                                break;
                            case "ID":
                                if (reader.Read()) ParameterID = Convert.ToInt32(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); return; }

            try
            {
                IrpinDBEntities DB = new IrpinDBEntities();

                var query = from queryPary in DB.MnemonicElements
                            where queryPary.StationID == Station
                            select queryPary;

                foreach (var tmp in query)
                {
                    var SetParameters = from queryParry1 in DB.SetParameters
                                        where queryParry1.MnemonicElementID == tmp.MnemonicElementID
                                        select queryParry1;

                    foreach (var Item in SetParameters)
                    {
                        if (Item.ParameterID == ParameterID) Item.Value = 0;
                    }
                }
                DB.SaveChanges();
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        public bool ReadXml(ref int ID, ref int Station, ref int Value, string xml)
        {
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(xml));
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "ID":
                                if (reader.Read()) ID = Convert.ToInt32(reader.Value.Trim());
                                break;
                            case "Station":
                                if (reader.Read()) Station = Convert.ToInt32(reader.Value.Trim());
                                break;
                            case "Value":
                                if (reader.Read()) Value = Convert.ToInt32(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { return false; }
            return true;
        }

        private void ReadDevice(string xml)
        {
            int ParameterID = 0, Station = 0, Value = 0;
            if (ReadXml(ref ParameterID, ref Station, ref Value, xml)) WriteToDB(ParameterID, Station, Value, xml);
        }

        public void WriteToDB(int ParameterID, int Station, int Value, string name)
        {
            double value = 0;
            
            try
            {
                IrpinDBEntities DB = new IrpinDBEntities();
                if (name.Contains("pulseCounter"))
                {
                    var request = from requerParry in DB.CubesBaseValue
                                  where requerParry.StationID == Station
                                  select requerParry;

                    foreach (var a in request)
                    {
                        value = Convert.ToDecimal(a.BaseValue);
                        Console.WriteLine("BaseValue :" + value);
                    }
                    Data.Add(new DataParameter(Station, 104, 0, Convert.ToDouble(value + Value * 0.1), StationEquipment.EquipmentTypeId));
                }
                if (name.Contains("Starter")) Data.Add(new DataParameter(Station, 105, 0, Convert.ToDouble(Value), StationEquipment.EquipmentTypeId));
                if (name.Contains("MoveSensor")) Data.Add(new DataParameter(Station, 106, 0, Convert.ToDouble(Value), StationEquipment.EquipmentTypeId));
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); };
        }

        private void ReadNIK(string xml)
        {
            String ID = String.Empty, Station = String.Empty;
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(xml));
                String value = String.Empty;

                lock (thisLock)
                {
                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name)
                            {
                                case "ID":
                                    if (reader.Read()) ID = reader.Value.Trim();
                                    break;
                                case "Station":
                                    if (reader.Read()) Station = reader.Value.Trim();
                                    break;
                                case "Voltage1":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 97, 0, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Voltage2":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 97, 1, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Voltage3":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 97, 2, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Amper1":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 3, 0, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Amper2":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 3, 1, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Amper3":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 3, 2, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Power1":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 2, 0, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Power2":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 2, 1, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "Power3":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 2, 2, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "SummaryPower":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 42, 0, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                                case "ActivePower":
                                    if (reader.Read()) value = reader.Value.Trim();
                                    Data.Add(new DataParameter(Convert.ToInt32(Station), 85, 0, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); return; }
        }

        public void ReleWork(string content, Socket handler)
        {
            int rele1 = 0, rele2 = 0, Station = 0, ParameterID1 = 0, ParameterID2 = 0;

            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(content));
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Station":
                                if (reader.Read()) Station = Convert.ToInt32(reader.Value.Trim());
                                break;
                            case "ParameterID1":
                                if (reader.Read()) ParameterID1 = Convert.ToInt32(reader.Value.Trim());
                                break;
                            case "ParameterID2":
                                if (reader.Read()) ParameterID2 = Convert.ToInt32(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); return; }

            try
            {
                IrpinDBEntities DB = new IrpinDBEntities();

                var query = from queryPary in DB.MnemonicElements
                            where queryPary.StationID == Station
                            select queryPary;

                foreach (var tmp in query)
                {
                    var SetParameters = from queryParry1 in DB.SetParameters
                                        where queryParry1.MnemonicElementID == tmp.MnemonicElementID
                                        select queryParry1;
                    foreach (var Item in SetParameters)
                    {
                        if (Item.ParameterID == ParameterID1 && Item.Value == 1) rele1 = 1;
                        if (Item.ParameterID == ParameterID2 && Item.Value == 1) rele2 = 1;
                    }
                    DB.SaveChanges();
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); return; }

            using (XmlTextWriter writer = new XmlTextWriter("Reles.xml", System.Text.Encoding.UTF8))
            {
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartElement("Reles");
                writer.WriteStartElement("Rele1");
                writer.WriteString(Convert.ToString(rele1));
                writer.WriteEndElement();
                writer.WriteStartElement("Rele2");
                writer.WriteString(Convert.ToString(rele2));
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
            string xml = File.ReadAllText("Reles.xml");
            Send(handler, xml);
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par;
            lock (thisLock) { par = new List<DataParameter>(Data); }
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else return Data;
        }
    }
}
